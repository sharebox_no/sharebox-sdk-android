package no.sharebox.api.exceptions;

public class ShareboxHttpException extends ShareboxException {

    private int code;

    public ShareboxHttpException(int code) {
        super("HTTP error code: " + code);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}
