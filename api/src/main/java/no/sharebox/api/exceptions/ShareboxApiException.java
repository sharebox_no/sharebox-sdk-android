package no.sharebox.api.exceptions;

public class ShareboxApiException extends ShareboxException {

    private int code;
    private String error;

    public ShareboxApiException(int code, String error) {
        super("API error: " + code + " - " + error);
        this.code = code;
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    @SuppressWarnings("unused") // Public API
    public String getError() {
        return error;
    }

}
