package no.sharebox.api.domain;

@SuppressWarnings("unused") // Public API
public class UserProfile {

    final String firstName;
    final String lastName;
    final String email;
    final String phone;
    final Status status;
    final boolean consentGranted;

    UserProfile(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.phone = builder.phone;
        this.status = builder.status;
        this.consentGranted = builder.consentGranted;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Status getStatus() {
        return status;
    }

    /**
     * Returns true if user granted their consent to latest terms version or false if user needs to
     * grant or update their consent.
     */
    public boolean isConsentGranted() {
        return consentGranted;
    }


    public static class Builder {

        String firstName;
        String lastName;
        String email;
        String phone;
        Status status;
        boolean consentGranted;

        public Builder() {}

        public Builder(UserProfile profile) {
            this.firstName = profile.firstName;
            this.lastName = profile.lastName;
            this.email = profile.email;
            this.phone = profile.phone;
            this.status = profile.status;
            this.consentGranted = profile.consentGranted;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder status(Status status) {
            this.status = status;
            return this;
        }

        public Builder consentGranted(boolean consentGranted) {
            this.consentGranted = consentGranted;
            return this;
        }

        public UserProfile build() {
            return new UserProfile(this);
        }

    }


    public enum Status {
        OK, BLOCKED
    }

}
