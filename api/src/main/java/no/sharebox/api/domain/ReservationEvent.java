package no.sharebox.api.domain;

import android.support.annotation.NonNull;

public class ReservationEvent {

    final Type type;
    final long date;
    final ReservationUser user;
    final boolean isByYou;

    ReservationEvent(Builder builder) {
        this.type = builder.type;
        this.date = builder.date;
        this.user = builder.user;
        this.isByYou = builder.isByYou;

        if (type == null) {
            throw new NullPointerException("Reservation event type cannot be null");
        }
    }

    @NonNull
    public Type getType() {
        return type;
    }

    public long getDate() {
        return date;
    }

    public ReservationUser getUser() {
        return user;
    }

    @SuppressWarnings("unused") // Public API
    public boolean isByYou() {
        return isByYou;
    }


    public static class Builder {

        Type type;
        long date;
        ReservationUser user;
        boolean isByYou;

        public Builder() {}

        public Builder(ReservationEvent event) {
            this.type = event.type;
            this.date = event.date;
            this.user = event.user;
            this.isByYou = event.isByYou;
        }

        public Builder type(@NonNull Type type) {
            this.type = type;
            return this;
        }

        public Builder date(long date) {
            this.date = date;
            return this;
        }

        public Builder user(ReservationUser user) {
            this.user = user;
            return this;
        }

        public Builder byYou(boolean isByYou) {
            this.isByYou = isByYou;
            return this;
        }

        public ReservationEvent build() {
            return new ReservationEvent(this);
        }

    }


    public enum Type {
        CREATED, OPENED
    }

}
