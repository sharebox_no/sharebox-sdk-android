package no.sharebox.api.domain;

@SuppressWarnings("WeakerAccess") // Public API
public class Cabinet {

    final long id;
    final boolean isOnline;
    final boolean hasFreeLockers;

    Cabinet(Builder builder) {
        this.id = builder.id;
        this.isOnline = builder.isOnline;
        this.hasFreeLockers = builder.hasFreeLockers;
    }

    public long getId() {
        return id;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public boolean hasFreeLockers() {
        return hasFreeLockers;
    }


    @Override
    public boolean equals(Object obj) {
        return this == obj
                || (obj != null && getClass() == obj.getClass() && id == ((Cabinet) obj).id);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


    public static class Builder {

        long id;
        boolean isOnline;
        boolean hasFreeLockers;

        public Builder() {}

        public Builder(Cabinet cabinet) {
            this.id = cabinet.id;
            this.isOnline = cabinet.isOnline;
            this.hasFreeLockers = cabinet.hasFreeLockers;
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder online(boolean online) {
            isOnline = online;
            return this;
        }

        public Builder hasFreeLockers(boolean hasFreeLockers) {
            this.hasFreeLockers = hasFreeLockers;
            return this;
        }

        public Cabinet build() {
            return new Cabinet(this);
        }

    }

}
