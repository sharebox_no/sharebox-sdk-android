package no.sharebox.api.domain;

public class Consent {

    final int versionCode;
    final String versionName;
    final String content;

    Consent(Builder builder) {
        this.versionCode = builder.versionCode;
        this.versionName = builder.versionName;
        this.content = builder.content;
    }

    public int getVersionCode() {
        return versionCode;
    }

    @SuppressWarnings("unused") // Public API
    public String getVersionName() {
        return versionName;
    }

    /**
     * Consent text in HTML format.
     */
    public String getContent() {
        return content;
    }


    public static class Builder {

        int versionCode;
        String versionName;
        String content;

        public Builder() {}

        public Builder(Consent consent) {
            this.versionCode = consent.versionCode;
            this.versionName = consent.versionName;
            this.content = consent.content;
        }

        public Builder versionCode(int versionCode) {
            this.versionCode = versionCode;
            return this;
        }

        public Builder versionName(String versionName) {
            this.versionName = versionName;
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Consent build() {
            return new Consent(this);
        }

    }

}
