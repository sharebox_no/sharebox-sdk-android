package no.sharebox.api.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

@SuppressWarnings({ "unused", "WeakerAccess" }) // Public API
public class StoreLocation {

    // Ideally we should receive timezones dynamically for each location,
    // but as a fallback we can assume that all locations are under Central European Timezone
    public static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("CET");

    final long id;
    final List<Cabinet> cabinets;
    final double latitude;
    final double longitude;
    final String name;
    final String address;
    final String city;
    final String zipCode;
    final String country;
    final long chainId;
    final List<OpenHours> openHours;

    StoreLocation(Builder builder) {
        this.id = builder.id;
        this.cabinets = builder.cabinets == null
                ? Collections.<Cabinet>emptyList()
                : Collections.unmodifiableList(builder.cabinets);
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.name = builder.name;
        this.address = builder.address;
        this.city = builder.city;
        this.zipCode = builder.zipCode;
        this.country = builder.country;
        this.chainId = builder.chainId;
        this.openHours = builder.openHours == null
                ? Collections.<OpenHours>emptyList()
                : Collections.unmodifiableList(builder.openHours);
    }

    public long getId() {
        return id;
    }

    @NonNull
    public List<Cabinet> getCabinets() {
        return cabinets;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountry() {
        return country;
    }

    public long getChainId() {
        return chainId;
    }

    @NonNull
    public List<OpenHours> getOpenHours() {
        return openHours;
    }


    /**
     * Returns full store address.
     */
    public String getFullAddress() {
        boolean hasAddress = address != null && address.length() != 0;
        boolean hasZip = zipCode != null && zipCode.length() != 0;
        boolean hasCity = city != null && city.length() != 0;
        boolean hasCountry = country != null && country.length() != 0;

        StringBuilder builder = new StringBuilder();
        if (hasAddress) {
            builder.append(address);
            if (hasZip || hasCity || hasCountry) {
                builder.append(", ");
            }
        }
        if (hasZip) {
            builder.append(zipCode);
            if (hasCity) {
                builder.append(" ");
            } else if (hasCountry) {
                builder.append(", ");
            }
        }
        if (hasCity) {
            builder.append(city);
            if (hasCountry) {
                builder.append(", ");
            }
        }
        if (hasCountry) {
            builder.append(country);
        }
        return builder.toString();
    }


    /**
     * Returns first online cabinet or null if no online cabinets found.
     */
    public Cabinet findAvailableCabinet() {
        for (Cabinet cabinet : cabinets) {
            if (cabinet.isOnline() && cabinet.hasFreeLockers()) {
                return cabinet;
            }
        }
        return null;
    }

    /**
     * Returns open hours for specific week day or null if store is closed at this day.
     */
    @Nullable
    public OpenHours findOpenHours(int weekday) {
        for (OpenHours hours : openHours) {
            if (hours.getWeekday() == weekday) {
                return hours;
            }
        }
        return null;
    }

    /**
     * Returns open hours for today or null if store is closed today.
     */
    @Nullable
    public OpenHours findOpenHoursToday() {
        Calendar calendar = Calendar.getInstance(DEFAULT_TIMEZONE);
        calendar.setTimeInMillis(System.currentTimeMillis());
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);
        return findOpenHours(weekday);
    }


    @Override
    public boolean equals(Object obj) {
        return this == obj
                || (obj != null && getClass() == obj.getClass() && id == ((StoreLocation) obj).id);
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


    public static class Builder {

        long id;
        List<Cabinet> cabinets;
        double latitude;
        double longitude;
        String name;
        String address;
        String city;
        String zipCode;
        String country;
        long chainId;
        List<OpenHours> openHours;

        public Builder() {}

        public Builder(StoreLocation location) {
            this.id = location.id;
            this.cabinets = location.cabinets;
            this.latitude = location.latitude;
            this.longitude = location.longitude;
            this.name = location.name;
            this.address = location.address;
            this.city = location.city;
            this.zipCode = location.zipCode;
            this.country = location.country;
            this.chainId = location.chainId;
            this.openHours = location.openHours;
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder cabinets(List<Cabinet> cabinets) {
            this.cabinets = cabinets;
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder zipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder chainId(long chainId) {
            this.chainId = chainId;
            return this;
        }

        public Builder openHours(List<OpenHours> openHours) {
            this.openHours = openHours;
            return this;
        }

        public StoreLocation build() {
            return new StoreLocation(this);
        }

    }

}
