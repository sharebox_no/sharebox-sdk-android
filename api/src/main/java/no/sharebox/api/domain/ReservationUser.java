package no.sharebox.api.domain;

import android.support.annotation.Nullable;

public class ReservationUser {

    final String name;
    final String phone;

    ReservationUser(Builder builder) {
        this.name = builder.name;
        this.phone = builder.phone;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }


    public static class Builder {

        String name;
        String phone;

        public Builder() {}

        public Builder(ReservationUser user) {
            this.name = user.name;
            this.phone = user.phone;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public ReservationUser build() {
            return new ReservationUser(this);
        }

    }

}
