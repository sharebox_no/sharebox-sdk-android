package no.sharebox.api.methods.responses;

public class RegisterData {

    private final String sessionId;

    public RegisterData(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

}
