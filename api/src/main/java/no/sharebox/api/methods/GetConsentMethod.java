package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.Consent;
import no.sharebox.api.methods.converters.ConsentConverter;

public class GetConsentMethod extends ApiJsonMethod<Void, Consent> {

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return "/v2/sharebox_get_consent";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull Void params) throws JSONException {
        // No body
    }

    @Nullable
    @Override
    Consent parseBody(@NonNull JSONObject json) throws JSONException {
        return ConsentConverter.convert(json);
    }

}
