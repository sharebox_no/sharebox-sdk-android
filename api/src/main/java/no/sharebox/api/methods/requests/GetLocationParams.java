package no.sharebox.api.methods.requests;

public class GetLocationParams {

    private final long locationId;

    GetLocationParams(Builder builder) {
        this.locationId = builder.locationId;
    }

    public long getLocationId() {
        return locationId;
    }


    public static class Builder {

        long locationId;

        /**
         * @param locationId <b>Required.</b> Id of a reservation to be fetched.
         */
        public Builder locationId(long locationId) {
            this.locationId = locationId;
            return this;
        }

        public GetLocationParams build() {
            return new GetLocationParams(this);
        }

    }

}
