package no.sharebox.api.methods.requests;

import no.sharebox.api.ShareboxApi;

public class VerifySmsCodeParams {

    private final String phone;
    private final String deviceId;
    private final String code;

    VerifySmsCodeParams(Builder builder) {
        this.phone = builder.phone;
        this.deviceId = builder.deviceId;
        this.code = builder.code;
    }

    public String getPhone() {
        return phone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getCode() {
        return code;
    }


    public static class Builder {

        String phone;
        String deviceId;
        String code;

        /**
         * @param phone <b>Required.</b> Phone number to be verified, same as sent in
         * {@link ShareboxApi#verifyPhone(VerifyPhoneParams)} method.
         */
        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        /**
         * @param deviceId <b>Required.</b> Unique device id, same as sent in
         * {@link ShareboxApi#verifyPhone(VerifyPhoneParams)} method.
         */
        public Builder deviceId(String deviceId) {
            this.deviceId = deviceId;
            return this;
        }

        /**
         * @param code <b>Required.</b> SMS verification code.
         */
        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public VerifySmsCodeParams build() {
            return new VerifySmsCodeParams(this);
        }

    }

}
