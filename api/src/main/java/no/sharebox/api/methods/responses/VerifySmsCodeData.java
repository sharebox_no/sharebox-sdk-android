package no.sharebox.api.methods.responses;

import no.sharebox.api.ShareboxApi;
import no.sharebox.api.methods.requests.LoginParams;
import no.sharebox.api.methods.requests.RegisterParams;

public class VerifySmsCodeData {

    private final String phoneToken;

    public VerifySmsCodeData(String phoneToken) {
        this.phoneToken = phoneToken;
    }

    /**
     * @return Phone verification token to be used in {@link ShareboxApi#login(LoginParams)}
     * or {@link ShareboxApi#register(RegisterParams)} methods.
     */
    public String getPhoneToken() {
        return phoneToken;
    }

}
