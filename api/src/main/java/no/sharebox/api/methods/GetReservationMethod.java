package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.ReservationEvent;
import no.sharebox.api.domain.ReservationUser;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.methods.converters.ReservationConverter;
import no.sharebox.api.methods.converters.StoreLocationConverter;
import no.sharebox.api.methods.requests.GetReservationParams;
import no.sharebox.api.methods.responses.GetReservationData;

public class GetReservationMethod extends ApiJsonMethod<GetReservationParams, GetReservationData> {

    @Override
    public String getPath() {
        return "/v2/sharebox_get_reservation_details";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull GetReservationParams params)
            throws JSONException {
        json.put("reservation_id", params.getReservationId());
    }

    @Nullable
    @Override
    GetReservationData parseBody(@NonNull JSONObject json) throws Exception {
        StoreLocation location = StoreLocationConverter.convertSingle(
                json.getJSONObject("location"), false);

        List<ReservationUser> recipients = ReservationConverter.convertRecipients(
                json.getJSONArray("members"));

        Reservation reservation = ReservationConverter.convertSingle(
                json.getJSONObject("details"), location, recipients);

        List<ReservationEvent> events = ReservationConverter.convertEvents(
                json.getJSONArray("events"));

        return new GetReservationData(reservation, events);
    }

}
