package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import no.sharebox.api.methods.requests.OpenLockerParams;

public class OpenLockerMethod extends ApiJsonMethod<OpenLockerParams, Void> {

    @Override
    public String getPath() {
        return "/v2/sharebox_open_locker";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull OpenLockerParams params) throws Exception {
        json.put("locker_id", params.getLockerId());
        json.put("latitude", params.getLatitude());
        json.put("longitude", params.getLongitude());
        json.put("flag", params.isForceOpen() ? 1 : 0);
        json.put("finish_flag", params.isFinishReservation() ? 1 : 0);
    }

    @Nullable
    @Override
    Void parseBody(@NonNull JSONObject json) throws Exception {
        return null;
    }

}
