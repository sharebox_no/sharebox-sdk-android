package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.methods.converters.StoreLocationConverter;

public class GetLocationsMethod extends ApiJsonMethod<Void, List<StoreLocation>> {

    @Override
    public String getPath() {
        return "/v2/sharebox_get_locations";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull Void params) throws JSONException {
        // No extra params
    }

    @Nullable
    @Override
    List<StoreLocation> parseBody(@NonNull JSONObject json) throws JSONException {
        return StoreLocationConverter.convertList(json.getJSONArray("locations"));
    }

}
