package no.sharebox.api.methods.requests;

public class GrantConsentParams {

    private final int versionCode;

    GrantConsentParams(Builder builder) {
        this.versionCode = builder.versionCode;
    }

    public int getVersionCode() {
        return versionCode;
    }


    public static class Builder {

        int versionCode;

        /**
         * @param versionCode <b>Required.</b> Consent version code.
         */
        public Builder versionCode(int versionCode) {
            this.versionCode = versionCode;
            return this;
        }

        public GrantConsentParams build() {
            return new GrantConsentParams(this);
        }

    }

}
