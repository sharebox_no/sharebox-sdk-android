package no.sharebox.api.methods.responses;

import no.sharebox.api.domain.UserProfile;

public class LoginData {

    private final String sessionId;
    private final UserProfile profile;

    public LoginData(String sessionId, UserProfile profile) {
        this.sessionId = sessionId;
        this.profile = profile;
    }

    public String getSessionId() {
        return sessionId;
    }

    public UserProfile getProfile() {
        return profile;
    }

}
