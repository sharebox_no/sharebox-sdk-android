package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.VerifyPhoneParams;

public class VerifyPhoneMethod extends ApiJsonMethod<VerifyPhoneParams, Void> {

    @Override
    public String getPath() {
        return "/v2/sharebox_authenticate_phone_number";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull VerifyPhoneParams params)
            throws JSONException {
        json.put("mobile", params.getPhone());
        json.put("device_id", params.getDeviceId());
        json.put("consent_version", params.getConsentVersion());
    }

    @Nullable
    @Override
    Void parseBody(@NonNull JSONObject json) throws JSONException {
        return null;
    }

}
