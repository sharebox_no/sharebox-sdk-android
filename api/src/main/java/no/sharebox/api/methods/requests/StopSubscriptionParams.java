package no.sharebox.api.methods.requests;

public class StopSubscriptionParams {

    private final long subscriptionId;

    StopSubscriptionParams(Builder builder) {
        this.subscriptionId = builder.subscriptionId;
    }

    public long getSubscriptionId() {
        return subscriptionId;
    }


    public static class Builder {

        long subscriptionId;

        /**
         * @param subscriptionId <b>Required.</b> Id of a subscription to be stopped.
         */
        public Builder subscriptionId(long subscriptionId) {
            this.subscriptionId = subscriptionId;
            return this;
        }

        public StopSubscriptionParams build() {
            return new StopSubscriptionParams(this);
        }

    }

}
