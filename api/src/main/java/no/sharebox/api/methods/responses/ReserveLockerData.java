package no.sharebox.api.methods.responses;

public class ReserveLockerData {

    private final long reservationId;

    public ReserveLockerData(long reservationId) {
        this.reservationId = reservationId;
    }

    @SuppressWarnings("unused") // Public API
    public long getReservationId() {
        return reservationId;
    }

}
