package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.ReserveLockerParams;
import no.sharebox.api.methods.responses.ReserveLockerData;

public class ReserveLockerMethod extends ApiJsonMethod<ReserveLockerParams, ReserveLockerData> {

    @Override
    public String getPath() {
        return "/v2/sharebox_reserve_locker";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull ReserveLockerParams params) throws JSONException {
        json.put("cabinet_id", params.getCabinetId());
        json.put("content", "other");
        json.put("reservation_type", 3);
        json.put("receiving_mob_numbers", new JSONArray().put(params.getReceiverPhone()));
        json.put("name", params.getReservationName());
    }

    @Nullable
    @Override
    ReserveLockerData parseBody(@NonNull JSONObject json) throws JSONException {
        return new ReserveLockerData(json.getLong("reservation_id"));
    }

}
