package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.methods.requests.GrantConsentParams;

public class GrantConsentMethod extends ApiJsonMethod<GrantConsentParams, Void> {

    @Override
    public String getPath() {
        return "/v2/sharebox_accept_consent";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull GrantConsentParams params)
            throws JSONException {
        json.put("version_code", params.getVersionCode());
    }

    @Nullable
    @Override
    Void parseBody(@NonNull JSONObject json) throws JSONException {
        return null;
    }

}
