package no.sharebox.api.methods.converters;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.Consent;

public class ConsentConverter {

    private ConsentConverter() {}

    public static Consent convert(JSONObject json) throws JSONException {
        return new Consent.Builder()
                .versionCode(json.getInt("version_code"))
                .versionName(json.getString("version"))
                .content(json.getString("content"))
                .build();
    }

}
