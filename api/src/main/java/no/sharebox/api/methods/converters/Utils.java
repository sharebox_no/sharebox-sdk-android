package no.sharebox.api.methods.converters;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

class Utils {

    private static final ThreadLocal<SimpleDateFormat> formatter =
            new ThreadLocal<SimpleDateFormat>() {
                @Override
                protected SimpleDateFormat initialValue() {
                    return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
                }
            };

    private Utils() {}

    static String optString(JSONObject json, String key) {
        Object value = json.opt(key);
        return value == null || value == JSONObject.NULL ? null : value.toString();
    }

    static boolean optBoolean(JSONObject json, String key) {
        int value = json.optInt(key, -1);
        if (value == -1) {
            return json.optBoolean(key);
        } else {
            return value != 0;
        }
    }

    /**
     * Converts date into milliseconds, or 0L if no date is found.
     */
    static long getDate(JSONObject json, String key) throws ParseException {
        String date = optString(json, key);
        if (date == null) {
            throw new NullPointerException("No date found for key: " + key);
        }
        return formatter.get().parse(date.replace("Z", "+0000")).getTime();
    }

    static String optFullName(JSONObject json, String firstKey, String lastKey) {
        String first = optString(json, firstKey);
        String last = optString(json, lastKey);
        return first == null || first.length() == 0
                ? null : (last == null || last.length() == 0 ? first : first + " " + last);
    }

}
