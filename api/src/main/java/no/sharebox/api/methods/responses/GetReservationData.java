package no.sharebox.api.methods.responses;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.ReservationEvent;

public class GetReservationData {

    private final Reservation reservation;
    private final List<ReservationEvent> events;

    public GetReservationData(@NonNull Reservation reservation,
            @NonNull List<ReservationEvent> events) {
        this.reservation = reservation;
        this.events = Collections.unmodifiableList(events);
    }

    @NonNull
    public Reservation getReservation() {
        return reservation;
    }

    @NonNull
    public List<ReservationEvent> getEvents() {
        return events;
    }

}
