package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.UserProfile;
import no.sharebox.api.methods.converters.UserProfileConverter;
import no.sharebox.api.methods.requests.LoginParams;
import no.sharebox.api.methods.responses.LoginData;

public class LoginMethod extends ApiJsonMethod<LoginParams, LoginData> {

    @Override
    public String getPath() {
        return "/sharebox_login";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull LoginParams params) throws JSONException {
        json.put("digits_id", params.getPhoneToken());
    }

    @Nullable
    @Override
    LoginData parseBody(@NonNull JSONObject json) throws JSONException {
        String sessionId = json.getString("session_id"); // Must be there
        UserProfile profile = UserProfileConverter.convert(json);
        return new LoginData(sessionId, profile);
    }

}
