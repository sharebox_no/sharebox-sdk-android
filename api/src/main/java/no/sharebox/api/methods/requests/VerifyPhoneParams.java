package no.sharebox.api.methods.requests;

import no.sharebox.api.ShareboxApi;

public class VerifyPhoneParams {

    private final String phone;
    private final String deviceId;
    private final int consentVersion;

    VerifyPhoneParams(Builder builder) {
        this.phone = builder.phone;
        this.deviceId = builder.deviceId;
        this.consentVersion = builder.consentVersion;
    }

    public String getPhone() {
        return phone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public int getConsentVersion() {
        return consentVersion;
    }


    public static class Builder {

        String phone;
        String deviceId;
        int consentVersion;

        /**
         * @param phone <b>Required.</b> Phone number to be verified.
         */
        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        /**
         * @param deviceId <b>Required.</b> Unique device id.
         */
        public Builder deviceId(String deviceId) {
            this.deviceId = deviceId;
            return this;
        }

        /**
         * @param consentVersion <b>Required.</b> Granted consent version for current user.
         * @see ShareboxApi#getConsent()
         */
        public Builder consentVersion(int consentVersion) {
            this.consentVersion = consentVersion;
            return this;
        }

        public VerifyPhoneParams build() {
            return new VerifyPhoneParams(this);
        }

    }

}
