package no.sharebox.api.methods.requests;

public class GetReservationParams {

    private final long reservationId;

    GetReservationParams(Builder builder) {
        this.reservationId = builder.reservationId;
    }

    public long getReservationId() {
        return reservationId;
    }


    public static class Builder {

        long reservationId;

        /**
         * @param reservationId <b>Required.</b> Id of a reservation to be fetched.
         */
        public Builder reservationId(long reservationId) {
            this.reservationId = reservationId;
            return this;
        }

        public GetReservationParams build() {
            return new GetReservationParams(this);
        }

    }

}
