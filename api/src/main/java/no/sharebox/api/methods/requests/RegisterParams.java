package no.sharebox.api.methods.requests;

import no.sharebox.api.ShareboxApi;

public class RegisterParams {

    private final String phoneToken;
    private final String firstName;
    private final String lastName;
    private final String email;

    RegisterParams(Builder builder) {
        this.phoneToken = builder.phoneToken;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
    }

    public String getPhoneToken() {
        return phoneToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }


    public static class Builder {

        String phoneToken;
        String firstName;
        String lastName;
        String email;

        /**
         * @param phoneToken <b>Required.</b> Phone verification token as returned by
         * {@link ShareboxApi#verifySmsCode(VerifySmsCodeParams)} method.
         */
        public Builder phoneToken(String phoneToken) {
            this.phoneToken = phoneToken;
            return this;
        }

        /**
         * @param firstName <b>Required.</b> User first name.
         */
        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        /**
         * @param lastName <b>Required.</b> User last name.
         */
        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        /**
         * @param email <b>Required.</b> User email address.
         */
        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public RegisterParams build() {
            return new RegisterParams(this);
        }

    }

}
