package no.sharebox.api.methods.converters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import no.sharebox.api.domain.Cabinet;
import no.sharebox.api.domain.OpenHours;
import no.sharebox.api.domain.StoreLocation;

public class StoreLocationConverter {

    @NonNull
    public static List<StoreLocation> convertList(@Nullable JSONArray json) {
        if (json == null) {
            return Collections.emptyList();
        }

        List<StoreLocation> locations = new ArrayList<>(json.length());
        for (int i = 0, size = json.length(); i < size; i++) {
            try {
                // Converting each location in the list ignoring cabinets' statuses, cabinets'
                // statuses should be requested manually to ensure we have latest info.
                locations.add(convertSingle(json.getJSONObject(i), false));
            } catch (Exception e) {
                Log.e("Sharebox", "Cannot convert store location", e);
            }
        }
        return Collections.unmodifiableList(locations);
    }

    @NonNull
    public static StoreLocation convertSingle(@NonNull JSONObject json, boolean withStatus)
            throws JSONException {
        return new StoreLocation.Builder()
                .id(json.getLong("location_id"))
                .cabinets(convertCabinets(json.optJSONArray("cabinets"), withStatus))
                .latitude(json.getDouble("latitude"))
                .longitude(json.getDouble("longitude"))
                .name(Utils.optString(json, "name"))
                .address(Utils.optString(json, "address"))
                .city(Utils.optString(json, "city"))
                .zipCode(Utils.optString(json, "zipcode"))
                .country(Utils.optString(json, "country"))
                .chainId(json.getLong("store_chain_id"))
                .openHours(convertHoursList(json.getJSONArray("opening_hours")))
                .build();
    }

    @NonNull
    private static List<Cabinet> convertCabinets(@Nullable JSONArray json, boolean withStatus)
            throws JSONException {
        if (json == null) {
            return Collections.emptyList();
        }

        ArrayList<Cabinet> cabinets = new ArrayList<>(json.length());
        for (int i = 0, size = json.length(); i < size; i++) {
            cabinets.add(convertCabinet(json.getJSONObject(i), withStatus));
        }
        return cabinets;
    }

    @NonNull
    private static Cabinet convertCabinet(@NonNull JSONObject json, boolean withStatus)
            throws JSONException {
        return new Cabinet.Builder()
                .id(json.getLong("cabinet_id"))
                .online(!withStatus || Utils.optBoolean(json, "online"))
                .hasFreeLockers(!withStatus || json.getInt("free_lockers") > 0)
                .build();
    }

    @NonNull
    private static List<OpenHours> convertHoursList(@Nullable JSONArray json) throws JSONException {
        if (json == null) {
            return Collections.emptyList();
        }

        ArrayList<OpenHours> hours = new ArrayList<>(json.length());
        for (int i = 0, size = json.length(); i < size; i++) {
            OpenHours item = convertHours(json.getJSONObject(i));
            if (item != null) {
                hours.add(item);
            }
        }
        return hours;
    }

    @Nullable
    private static OpenHours convertHours(@NonNull JSONObject json) throws JSONException {
        int weekday = convertWeekday(Utils.optString(json, "day_of_week"));
        int startMin = convertToMinutes(Utils.optString(json, "opening_hour"));
        int endMin = convertToMinutes(Utils.optString(json, "closing_hour"));

        if (weekday == -1 || startMin == -1L || endMin == -1L) {
            return null;
        }

        return new OpenHours.Builder()
                .weekday(weekday)
                .start(startMin / 60, startMin % 60)
                .end(endMin / 60, endMin % 60)
                .build();
    }

    private static int convertWeekday(String code) {
        if ("sun".equalsIgnoreCase(code)) {
            return Calendar.SUNDAY;
        } else if ("mon".equalsIgnoreCase(code)) {
            return Calendar.MONDAY;
        } else if ("tue".equalsIgnoreCase(code)) {
            return Calendar.TUESDAY;
        } else if ("wed".equalsIgnoreCase(code)) {
            return Calendar.WEDNESDAY;
        } else if ("thu".equalsIgnoreCase(code)) {
            return Calendar.THURSDAY;
        } else if ("fri".equalsIgnoreCase(code)) {
            return Calendar.FRIDAY;
        } else if ("sat".equalsIgnoreCase(code)) {
            return Calendar.SATURDAY;
        } else {
            return -1;
        }
    }

    private static int convertToMinutes(String timeStr) {
        if (timeStr == null) {
            return -1;
        }

        String[] parts = timeStr.split(":");
        if (parts.length != 3) {
            return -1;
        }

        try {
            return Integer.valueOf(parts[0]) * 60 + Integer.valueOf(parts[1]);
        } catch (Exception e) {
            return -1;
        }
    }

}
