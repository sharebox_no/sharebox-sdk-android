package no.sharebox.api.methods.converters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.ReservationEvent;
import no.sharebox.api.domain.ReservationUser;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.exceptions.ShareboxException;

public class ReservationConverter {

    private ReservationConverter() {}

    @NonNull
    public static List<Reservation> convertList(JSONArray json)
            throws JSONException, ParseException {
        if (json == null) {
            return Collections.emptyList();
        }

        List<Reservation> reservations = new ArrayList<>(json.length());
        for (int i = 0, size = json.length(); i < size; i++) {
            try {
                reservations.add(convertSingle(json.getJSONObject(i), null, null));
            } catch (Exception e) {
                Log.e("Sharebox", "Cannot convert reservation", e);
            }
        }
        return Collections.unmodifiableList(reservations);
    }

    @NonNull
    public static Reservation convertSingle(JSONObject json,
            @Nullable StoreLocation location, @Nullable List<ReservationUser> recipients)
            throws JSONException, ParseException {

        if (location == null) {
            location = StoreLocationConverter.convertSingle(json.getJSONObject("location"), false);
        }
        if (recipients == null) {
            recipients = Collections.singletonList(
                    convertRecipient(json.getJSONObject("receiver_first")));
        }

        return new Reservation.Builder()
                .id(json.getLong("id"))
                .date(Utils.getDate(json, "date"))
                .subscriptionId(json.getLong("subscription_id"))
                .name(Utils.optString(json, "subscription_name"))
                .fromYou(Utils.optBoolean(json, "reserved_by_you"))
                .owner(convertOwner(json))
                .recipients(recipients)
                .recipientsCount(json.getInt("receivers_count"))
                .location(location)
                .lockerId(json.getLong("locker_id"))
                .lockerNumber(json.getInt("locker_no"))
                .type(toType(json.getInt("reservation_type_id"), Utils.optBoolean(json, "is_free")))
                .active(isActive(json.getInt("status")))
                .open(Utils.optBoolean(json, "is_locker_open"))
                .freeHours(json.optInt("free_hours"))
                .build();
    }

    private static ReservationUser convertOwner(JSONObject json) {
        return new ReservationUser.Builder()
                .name(Utils.optFullName(json, "reserver_firstname", "reserver_lastname"))
                .phone(Utils.optString(json, "reserver_mobile"))
                .build();
    }

    private static ReservationUser convertRecipient(JSONObject json) {
        return new ReservationUser.Builder()
                .name(Utils.optString(json, "name"))
                .phone(Utils.optString(json, "mobile"))
                .build();
    }

    @NonNull
    public static List<ReservationUser> convertRecipients(JSONArray json) throws JSONException {
        if (json == null) {
            return Collections.emptyList();
        }

        List<ReservationUser> recipients = new ArrayList<>(json.length());
        for (int i = 0, size = json.length(); i < size; i++) {
            recipients.add(convertRecipient(json.getJSONObject(i)));
        }
        return recipients;
    }

    private static Reservation.Type toType(int typeCode, boolean isFree) {
        if (typeCode == 2) {
            return Reservation.Type.MONTHLY;
        } else if (typeCode == 3) {
            return isFree ? Reservation.Type.FREE : Reservation.Type.REGULAR;
        } else {
            throw new ShareboxException("Unknown reservation type: " + typeCode);
        }
    }

    private static boolean isActive(int status) {
        return status == 14;
    }



    /* ------------------- */
    /* Reservation events  */
    /* ------------------- */

    @NonNull
    public static List<ReservationEvent> convertEvents(JSONArray json) {
        if (json == null) {
            return Collections.emptyList();
        }

        try {
            List<ReservationEvent> events = new ArrayList<>(json.length());
            for (int i = 0, size = json.length(); i < size; i++) {
                ReservationEvent event = convertEvent(json.getJSONObject(i));
                if (event != null) {
                    events.add(event);
                }
            }
            return events;
        } catch (Exception e) {
            Log.e("Sharebox", "Cannot convert events list", e);
            return Collections.emptyList();
        }
    }

    @Nullable
    private static ReservationEvent convertEvent(JSONObject json)
            throws JSONException, ParseException {
        ReservationEvent.Type type = toEventType(json.getInt("event_type_id"));

        // Skipping unknown events
        if (type == null) {
            return null;
        }

        return new ReservationEvent.Builder()
                .type(type)
                .date(Utils.getDate(json, "registered"))
                .user(convertEventUser(json))
                .byYou(Utils.optBoolean(json, "opened_by_you"))
                .build();
    }

    private static ReservationUser convertEventUser(JSONObject json) {
        return new ReservationUser.Builder()
                .name(Utils.optFullName(json, "firstname", "lastname"))
                .phone(Utils.optString(json, "mobile"))
                .build();
    }

    private static ReservationEvent.Type toEventType(int typeId) {
        if (typeId == 6 || typeId == 18) {
            return ReservationEvent.Type.CREATED;
        } else if (typeId == 7 || typeId == 500) {
            return ReservationEvent.Type.OPENED;
        } else {
            return null;
        }
    }

}
