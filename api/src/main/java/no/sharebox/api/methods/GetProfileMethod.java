package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import no.sharebox.api.domain.UserProfile;
import no.sharebox.api.methods.converters.UserProfileConverter;

public class GetProfileMethod extends ApiJsonMethod<Void, UserProfile> {

    @Override
    public String getPath() {
        return "/sharebox_login";
    }

    @Override
    void createBody(@NonNull JSONObject json, @NonNull Void params) throws JSONException {
        // No extra params
    }

    @Nullable
    @Override
    UserProfile parseBody(@NonNull JSONObject json) throws JSONException {
        return UserProfileConverter.convert(json);
    }

}
