package no.sharebox.api.methods.requests;

import no.sharebox.api.ShareboxApi;

public class LoginParams {

    private final String phoneToken;

    LoginParams(Builder builder) {
        this.phoneToken = builder.phoneToken;
    }

    public String getPhoneToken() {
        return phoneToken;
    }


    public static class Builder {

        String phoneToken;

        /**
         * @param phoneToken <b>Required.</b> Phone verification token as returned by
         * {@link ShareboxApi#verifySmsCode(VerifySmsCodeParams)} method.
         */
        public Builder phoneToken(String phoneToken) {
            this.phoneToken = phoneToken;
            return this;
        }

        public LoginParams build() {
            return new LoginParams(this);
        }

    }

}
