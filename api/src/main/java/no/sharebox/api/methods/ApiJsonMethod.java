package no.sharebox.api.methods;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.io.IOException;

import no.sharebox.api.exceptions.ShareboxApiException;
import no.sharebox.api.exceptions.ShareboxException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

abstract class ApiJsonMethod<I, O> implements ApiMethod<I, O> {

    private static final MediaType CONTENT_TYPE_JSON = MediaType.parse("application/json");

    @Override
    public String getMethod() {
        return "POST";
    }

    @NonNull
    @Override
    public final RequestBody createBody(@Nullable I params, @Nullable String sessionId) {
        try {
            JSONObject json = new JSONObject();
            if (params != null) {
                createBody(json, params);
            }
            if (sessionId != null) {
                json.put("session_id", sessionId);
            }
            return RequestBody.create(CONTENT_TYPE_JSON, json.toString());
        } catch (Exception e) {
            throw new ShareboxException("Cannot convert request into JSON", e);
        }
    }

    @Nullable
    @Override
    public final O parseBody(@Nullable ResponseBody body) throws IOException {
        if (body == null) {
            return null;
        }

        final JSONObject json;
        try {
            json = new JSONObject(body.string());

            // Checking API error code
            final int errorCode = json.optInt("result_code");
            final String errorMessage = json.optString("error_message");
            if (errorCode != 0) {
                throw new ShareboxApiException(errorCode, errorMessage);
            }

            return parseBody(json);
        } catch (Exception e) {
            if (e instanceof ShareboxException) {
                throw (ShareboxException) e;
            } else {
                throw new ShareboxException("Cannot convert JSON from response", e);
            }
        }
    }

    abstract void createBody(@NonNull JSONObject json, @NonNull I params) throws Exception;

    @Nullable
    abstract O parseBody(@NonNull JSONObject json) throws Exception;

}
