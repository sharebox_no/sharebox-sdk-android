package no.sharebox.api.methods.converters;

import org.json.JSONObject;

import no.sharebox.api.domain.UserProfile;

public class UserProfileConverter {

    private UserProfileConverter() {}

    public static UserProfile convert(JSONObject json) {
        return new UserProfile.Builder()
                .firstName(Utils.optString(json, "first_name"))
                .lastName(Utils.optString(json, "last_name"))
                .email(Utils.optString(json, "email"))
                .phone(Utils.optString(json, "mobile"))
                .status(getStatusForCode(json.optInt("customer_status")))
                .consentGranted(!Utils.optBoolean(json, "must_provide_consent"))
                .build();
    }

    private static UserProfile.Status getStatusForCode(int code) {
        return code == 2 ? UserProfile.Status.BLOCKED : UserProfile.Status.OK;
    }

}
