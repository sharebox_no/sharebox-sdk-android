Sharebox Android SDK
============

[![Maven Central][mvn-img]][mvn-url]

Sharebox SDK allows app developers to integrate Sharebox® service into their native Android apps.

Sharebox simplifies your handover by offering a service completely managed by your phone.
App-controlled cabinets, placed at central locations, ensure you a safe and efficient handover
whenever you need.

Sharebox SDK provides functionality to book lockers for free for a limited period of time.
Users can book any lockers they want, however each free reservation will be converted into a paid
one once free period ends.

## Structure ##

This project is divided into 3 parts:

* __API module__ - plain Sharebox API implementation.
* __SDK module__ - advanced implementation on top of API module, includes basic storage,
login/registration flow, etc.
* __Sample app__ - simple app that demonstrates SDK usage and general service flow.


## Running sample app ##

To build and run sample app you will need to set up extra params in your local.properties file
in project's root directory:

```properties
sharebox.login=[API_LOGIN]
sharebox.password=[API_PASSWORD]
```

> _Do not forget to replace `[API_LOGIN]` and `[API_PASSWORD]` with actual credentials provided by
Sharebox._

You may also set up optional API endpoint url:

```properties
sharebox.base_url=[API base url, w/o leading slash]
```


## Usage ##

* [Dependency](#markdown-header-dependency)
* [Configuration](#markdown-header-configuration)
* [Async execution](#markdown-header-async-execution)
* [API errors](#markdown-header-api-errors)
* [Login flow](#markdown-header-login-flow)
* [Consent](#markdown-header-consent) 
* [Phone verification](#markdown-header-phone-verification)
* [Login](#markdown-header-login)
* [Registration](#markdown-header-registration)
* [Profile](#markdown-header-profile)
* [Logout](#markdown-header-logout)
* [Locations](#markdown-header-locations)
* [Location details](#markdown-header-location-details)
* [Booking](#markdown-header-booking)
* [Reservations list](#markdown-header-reservations-list)
* [Reservation details](#markdown-header-reservation-details)
* [Reservation actions](#markdown-header-reservation-actions)
* [User location](#markdown-header-user-location)
* [Useful constants](#markdown-header-useful-constants)


### Dependency ###

Add a new dependency into your app's build.gradle file:

```groovy
implementation 'no.sharebox:sdk:1.0.1'
```

If you only want to use API module then add the following dependency instead:

```groovy
implementation 'no.sharebox:api:1.0.1'
```

### Configuration ###

Now we need to initialize SDK in `Application.onCreate()` method:

```java
ShareboxSdk.init(this, new ShareboxApi.Options()
        .credentials("[API_LOGIN]", "[API_PASSWORD]")
        .debugLogs(BuildConfig.DEBUG));
```

> _Do not forget to replace `[API_LOGIN]` and `[API_PASSWORD]` with actual credentials provided by
Sharebox._

Once initialized `ShareboxSdk` instance can be retrieved with `ShareboxSdk.get()` method.

If you're only planing to use API module then initialize `ShareboxApi` directly:

```java
ShareboxApi.init(new ShareboxApi.Options()
        .credentials("[API_LOGIN]", "[API_PASSWORD]")
        .debugLogs(BuildConfig.DEBUG));
```

Once initialized `ShareboxApi` instance can be retrieved with `ShareboxApi.get()` method.


### Async execution ###

All SDK methods can be called from the main thread, but some of the methods should perform heavy
tasks (like calling API methods) and will actually be executed in background thread.

Such methods will return `Task` object instead of the result itself so that we can provide necessary
callbacks and request background execution. Available callbacks are:

* OnResultListener<T>
* OnErrorListener

A task will be executed only when we'll call `subscribe()` method on it.

Since tasks are executed in background thread we can potentially leak our callbacks references,
which in turn can contain references to current activity leading to memory leaks.
To avoid such leaks `subscribe()` method will return `TaskRequest` instance which should be used
to cancel execution when we are leaving current activity and don't want our callbacks to be called
anymore.

> _Note, that if current activity extends `android.support.v7.app.AppCompatActivity` (or fragment
extends `android.support.v4.app.Fragment`) from support lib v27+ it will implement `LifecycleOwner`
so we can automatically cancel a task using
`TaskRequest.cancelOnDestroy(LifecycleOwner lifecycleOwner)` method._

Therefore typical task execution will look like this:

```java
ShareboxSdk.get().updateProfile()
        .onResult(profile -> {
            // Handle the result
        })
        .onError(ex -> {
            // Handle the error
        })
        .subscribe()
        .cancelOnDestroy(this);
```


### API errors ###

Sharebox API may return predefined error codes if something goes wrong (ref. API specification),
in this case `ShareboxApiException` will be thrown and error code can be accessed with `getCode()`
method. Some of the most common API error codes can be found as `ShareboxApi.ERROR_*` constants.


### Login flow ###

Before the user can start using the service we need to authorize them in Sharebox.
But before logging in we also need to collect user's consent to Sharebox terms,
so the overall flow looks like this:

* [Consent](#markdown-header-consent) - Asking the user to provide their consent to Sharebox terms. 
* [Phone verification](#markdown-header-phone-verification) - Requesting SMS code to verify user's
phone number.
* [Login](#markdown-header-login) - Verifying SMS code and trying to log the user in.
* [Registration](#markdown-header-registration) - Registering the user, if not registered.


### Consent ###

To load Sharebox consent use the following SDK method:

```java
Task<Consent> getConsent();
```

> _Note, that this method returns `Task` instance, check
[Async execution](#markdown-header-async-execution) section for more information on how to deal
with callbacks._

The app must clearly present consent's text to the user and the user must explicitly accept it to
be able to use Sharebox service. The app must not grant consent on user behalf and must not
pre-check related checkbox or similar control.

Once the user grants their consent we should save version of the consent text
(`Consent.getVersionCode()`) to be used further.

### Phone verification ###

Once consent is collected we can proceed with phone verification:

```java
Task<Void> verifyPhone(String phone, int consentVersion);
```

> _Note, that provided phone number should be in international format starting with country code
prefixed with + sign and can only contain digits and the + sign itself. For example: `+4712345678`_

The user should now receive SMS with 5-digits code on this phone number to actually verify the phone.

We can check that SMS code was requested but was not yet verified with:

```java
boolean isVerificationRequested();
```

We can also access the phone number used for verification with:

```java
String getPhoneToVerify();
```


### Login ###

Once the user provided phone verification code received by SMS we can try to log them in:

```java
Task<Void> login(String code);
```

If the user was already registered in Sharebox then this method will be successfully executed and
the user will be authorized.

We can check that the user is logged in with:

```java
boolean isLoggedIn();
```

But if the user was not registered yet login method will return `ShareboxApiException` error with
code `ShareboxApi.ERROR_NOT_REGISTERED` and we should ask the user to register
(see [Registration](#markdown-header-registration) section).

We can check that the user successfully verified their phone number but didn't register yet with:

```java
boolean isRegistrationNeeded();
```

If SMS code provided by the user is invalid API will return `ShareboxApiException` error with code
`ShareboxApi.ERROR_INVALID_SMS_CODE`.


### Registration ###

If we detected that the user is not registered we should ask them to provide first name, last name
and email address and then register this information with:

```java
Task<Void> register(String firstName, String lastName, String email);
```

Once registered the user will be automatically authorized and we can start using Sharebox service.


### Profile ###

Once the user is logged in (or registered) we should already have profile information loaded
and cached locally.

To update user profile details from the server we can use the following method:

```java
Task<UserProfile> updateProfile();
```

To get cached profile details and subscribe to all subsequent profile updates we can use:

```java
TaskRequest getProfileUpdates(OnResultListener<UserProfile> listener);
```

> _See [Async execution](#markdown-header-async-execution) section for more information
about `TaskRequest` instance._


__Important: Consent__

Every time user profile is updated from the server we should check that we have user consent for
the latest version of Sharebox terms with `UserProfile.isConsentGranted()` method. If this method
returns `false` we should load consent text as described in [Consent](#markdown-header-consent)
section above, ask the user to accept it and then save it back into user profile with:

```java
Task<Void> grantConsent(int versionCode);
```

If the user does not accept new terms we should log them out from Sharebox. 

__Important: Blocked users__

The user can be blocked from using the service (for example the user forgot to pay for
a reservation), in this case they will not be allowed to create new reservations but will still be
able to load and control existing reservations.

If the user is blocked method `UserProfile.getStatus()` will return `BLOCKED` status.


### Logout ###

To log the user out and clear all user data saved locally we should use the following method:

```java
void logout();
```

This method can also be used at any point during login and registration flow to reset the progress.


### Locations ###

To start reservation process the user should pick one of the available locations within single
stores chain (as defined by API credentials).
Available locations list can be loaded with the following method:

```java
Task<List<StoreLocation>> getLocations();
```

By default locations list is sorted by name, but it is preferable to sort locations by distance.
See [User location](#markdown-header-user-location) section for information on how to get current
user location.

Once we know user location we can compute locations distances with:

```java
Map<StoreLocation, Integer> ShareboxUtils.computeDistances(List<StoreLocation> list, Location location);
```

And compute sorted locations list with:

```java
List<StoreLocation> ShareboxUtils.sortByDistance(Map<StoreLocation, Integer> distances);
```


### Location details ###

When the user picks particular location we should load it's details to check cabinets and boxes
availability for this location:

```java
Task<StoreLocation> getLocation(long locationId);
```

Location details loaded with this method will contain correct cabinets availability info which can
be checked with `StoreLocation.findAvailableCabinet()` method: if this method returns `null` then
we don't have available boxes in this location and the user should not be able to reserve boxes here.


### Booking ###

Once the user picked desired location and we checked availability for this location we can proceed
with box reservation.

The user needs to provide reservation name (for example description of the box content) and specify
phone number of the person they want to share the box with. Once we have all the required
information we can create a new reservation with:

```java
Task<ReserveLockerData> reserveLocker(long cabinetId, String reservationName, String phone)
```

Where `cabinetId` is ID of the cabinet found with `StoreLocation.findAvailableCabinet()` method.

> _Note, that provided phone number should be in international format starting with country code
prefixed with + sign and can only contain digits and the + sign itself. For example: `+4712345678`_

Once a new reservation is created we should provide basic instructions to the user and redirect them
to the reservation details. Next user's step will be to open the box and put their stuff into it.


### Reservations list ###

Active user reservations can be loaded with the following method:

```java
Task<List<Reservation>> getReservationsActive();
```

List of last several non-active reservations can be loaded with:

```java
Task<List<Reservation>> getReservationsCompleted();
```

And finally both these lists combined can be loaded (in parallel) with:

```java
Task<List<Reservation>> getReservationsAll();
```

> _Note, that each reservation may potentially have more than 1 recipient, but only first
recipient will be provided when loading reservations list, to load all recipients you will need
to separately load reservation details,
see [Reservation details](#markdown-header-reservation-details) section._


### Reservation details ###

We can use the following method to load single reservation details and to load a list of
corresponding reservation events (for example "reserved by" and "box opened by"):

```java
Task<GetReservationData> getReservation(long reservationId);
```


### Reservation actions ###

List of available reservation's actions depends on whether current user is the owner or
the recipient of the reservation.

Owner's actions:

* Open the box
* Finish the reservation
* Open the box and finish the reservation

Recipient's actions:

* Open the box (reservation will be finished automatically)

You can use the following methods to check if it is possible to perform any of these actions:

```java
boolean Reservation.canOpen();
boolean Reservation.canFinish();
```

We can use the following method to open the box (with or without finishing the reservation):

```java
Task<Void> openLocker(OpenLockerParams params);
```

Where method params are constructed like this:

```java
OpenLockerParams params = new OpenLockerParams.Builder()
        .lockerId(reservation.getLockerId())
        .location(lat, lon)
        .forceOpen(force)
        .finishReservation(finish)
        .build();
```

We can use the following method to finish the reservation:

```java
Task<Void> stopSubscription(long subscriptionId);
```

Where `subscriptionId` is ID of the reservation's subscription found with `Reservation.getSubscriptionId()` method.

__Important: user location__

When opening a locker we should send current user location so that the server can check that
the user is close enough to the locker to open it, otherwise API will return `ShareboxApiException`
error with code `ShareboxApi.ERROR_TOO_FAR_FROM_LOCKER`.

See [User location](#markdown-header-user-location) section for information on how to get current
user location.

If the user is not close enough or we don't have access to current user location then we still
can open the locker by setting `forceOpen(true)` in `OpenLockerParams`, but the user should be
notified about the problem and should explicitly agree to proceed with the action anyway. 


### User location ###

We need to know user location to sort locations list by their distances
(see [Locations](#markdown-header-locations) section) and when performing actions on reservations
(see [Reservation actions](#markdown-header-reservation-actions) section). SDK provides special
`LocationHelper` class which handles permissions and subscription logic for us.
 
To use the helper we need to add extra dependency into app's build.gradle file (you need to provide
latest version of [Google Play services](https://developers.google.com/android/guides/releases)):

```groovy
implementation 'com.google.android.gms:play-services-location:X.X.X'
```

And either `ACCESS_COARSE_LOCATION` or `ACCESS_FINE_LOCATION` should be added into app's manifest.

Now we can create and use an instance of Location helper as follows:

``` java
private LocationHelper locationHelper;

@Override
protected void onCreate(...) {
    locationHelper = new LocationHelper(this, location -> {
        // Handle user location
    });
    locationHelper.autoConnect();
}

@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {
    boolean handled = locationHelper.handlePermissionsResult(requestCode, grantResults);

    if (!handled) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
```

Few things to note here:

* We need to call `locationHelper.autoConnect()` to make sure that helper will automatically
subscribe to location updates on resume and unsubscribe on pause. If your Activity or Fragment does
not implement `LifecycleOwner` interface you will need to manually call `resume()` and `pause()`
methods.

* We also need to manually call `LocationHelper.handlePermissionsResult(...)` from
`onRequestPermissionsResult(...)` of your Activity or Fragment to properly handle location
permission requests.


### Useful constants ###

`ShareboxSdk` class provides few useful constants:

* `FREE_HOURS` - Number of free hours for each reservation
* `TERMS_URL` - Link to terms and conditions page.
* `SUPPORT_PHONE_NO` - Support phone number for Norway.
* `SUPPORT_PHONE_DK` - Support phone number for Denmark.


## License ##

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


[mvn-url]: https://maven-badges.herokuapp.com/maven-central/no.sharebox/sdk
[mvn-img]: https://img.shields.io/maven-central/v/no.sharebox/sdk.svg?style=flat-square

[api-javadoc-url]: http://javadoc.io/doc/no.sharebox/api
[sdk-javadoc-url]: http://javadoc.io/doc/no.sharebox/sdk
