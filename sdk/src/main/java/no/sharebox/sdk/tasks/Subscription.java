package no.sharebox.sdk.tasks;

import android.support.annotation.AnyThread;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Subscription<T> implements OnResultListener<T> {

    private final MainThreadHandler mainThread;
    private final List<Task<T>> tasks = new CopyOnWriteArrayList<>();

    private volatile boolean initialized;
    private volatile T result;

    public Subscription(MainThreadHandler mainThread) {
        this.mainThread = mainThread;
    }

    TaskRequest subscribe(final Task<T> task) {
        if (!tasks.contains(task)) {
            tasks.add(task);
        }
        if (initialized) {
            mainThread.deliverResult(task, result);
        }

        return new TaskRequest(new TaskCancellation() {
            @Override
            public void cancel() {
                tasks.remove(task); // Un-subscribing the task
                task.clear(); // Clearing listeners to prevent memory leaks
            }
        });
    }

    @AnyThread
    @Override
    public void onResult(@Nullable T result) {
        this.initialized = true;
        this.result = result;

        for (Task<T> task : tasks) {
            mainThread.deliverResult(task, result);
        }
    }

}
