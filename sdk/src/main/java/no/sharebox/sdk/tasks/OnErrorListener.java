package no.sharebox.sdk.tasks;

public interface OnErrorListener {

    void onError(Exception ex);

}
