package no.sharebox.sdk.tasks;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BackgroundExecutor {

    private static final int MAX_THREADS = 2;

    private final MainThreadHandler mainThread;
    private final ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);

    public BackgroundExecutor(MainThreadHandler mainThread) {
        this.mainThread = mainThread;
    }

    <T> TaskRequest execute(final Task<T> task, final Callable<T> callable) {
        final Future future = executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    mainThread.deliverResult(task, callable.call());
                } catch (Exception ex) {
                    mainThread.deliverError(task, ex);
                }
            }
        });

        return new TaskRequest(new TaskCancellation() {
            @Override
            public void cancel() {
                future.cancel(false); // Cancelling task if not running yet
                task.clear(); // Clearing listeners to prevent memory leaks
            }
        });
    }

}
