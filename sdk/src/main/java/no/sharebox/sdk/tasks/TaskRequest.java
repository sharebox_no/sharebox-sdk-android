package no.sharebox.sdk.tasks;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

public class TaskRequest {

    private final TaskCancellation cancellation;

    private Lifecycle lifecycle;
    private LifecycleObserver observer;

    TaskRequest(TaskCancellation cancellation) {
        this.cancellation = cancellation;
    }

    /**
     * Cancels this task and clean up all listeners references to avoid memory leaks.
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public void cancel() {
        cancellation.cancel(); // Cancelling task and clearing listeners to prevent memory leaks

        // Removing lifecycles observer if any
        if (observer != null) {
            lifecycle.removeObserver(observer);
            lifecycle = null;
            observer = null;
        }
    }

    /**
     * Automatically cancels this task and cleans up all listeners references to avoid memory leaks
     * when corresponding activity or fragment is destroyed.
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public void cancelOnDestroy(@NonNull Lifecycle lifecycle) {
        this.lifecycle = lifecycle;
        this.observer = new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            public void onDestroy() {
                cancel();
            }
        };

        lifecycle.addObserver(observer);
    }

    /**
     * Automatically cancels this task and cleans up all listeners references to avoid memory leaks
     * when corresponding activity or fragment is destroyed.
     *
     * @param lifecycleOwner {@link LifecycleOwner} instance, for example
     * {@link AppCompatActivity} or {@link Fragment}.
     */
    public void cancelOnDestroy(@NonNull LifecycleOwner lifecycleOwner) {
        cancelOnDestroy(lifecycleOwner.getLifecycle());
    }

}
