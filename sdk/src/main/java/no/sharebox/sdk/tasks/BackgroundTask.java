package no.sharebox.sdk.tasks;

import java.util.concurrent.Callable;

public class BackgroundTask<T> extends Task<T> {

    private final BackgroundExecutor executor;
    private final Callable<T> callable;

    public BackgroundTask(BackgroundExecutor executor, Callable<T> callable) {
        this.executor = executor;
        this.callable = callable;
    }

    /**
     * Asynchronously executes this task.
     */
    @Override
    public TaskRequest subscribe() {
        return executor.execute(this, callable);
    }

}
