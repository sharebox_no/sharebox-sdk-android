package no.sharebox.sdk.tasks;

public interface OnResultListener<R> {

    void onResult(R result);

}
