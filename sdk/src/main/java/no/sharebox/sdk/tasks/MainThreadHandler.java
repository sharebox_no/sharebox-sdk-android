package no.sharebox.sdk.tasks;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public final class MainThreadHandler {

    private static final int WHAT_RESULT = 0;
    private static final int WHAT_ERROR = 1;

    private final MainHandler mainThread = new MainHandler();


    <T> void deliverResult(Task<T> task, T result) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            task.deliverResult(result);
        } else {
            mainThread.obtainMessage(WHAT_RESULT, new Object[] { task, result }).sendToTarget();
        }
    }

    <T> void deliverError(Task<T> task, Exception ex) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            task.deliverError(ex);
        } else {
            mainThread.obtainMessage(WHAT_ERROR, new Object[] { task, ex }).sendToTarget();
        }
    }


    private static class MainHandler extends Handler {
        MainHandler() {
            super(Looper.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            Object[] data = (Object[]) msg.obj;
            Task task = (Task) data[0];
            Object value = data[1];

            switch (msg.what) {
                case WHAT_RESULT:
                    //noinspection unchecked - We should be fine here, but we can't cast to T
                    task.deliverResult(value);
                    break;
                case WHAT_ERROR:
                    task.deliverError((Exception) value);
                    break;
                default:
            }
        }
    }

}
