package no.sharebox.sdk;

import android.content.Context;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import no.sharebox.api.ShareboxApi;
import no.sharebox.api.domain.Consent;
import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.api.domain.UserProfile;
import no.sharebox.api.exceptions.ShareboxApiException;
import no.sharebox.api.exceptions.ShareboxException;
import no.sharebox.api.methods.requests.GetLocationParams;
import no.sharebox.api.methods.requests.GetReservationParams;
import no.sharebox.api.methods.requests.GrantConsentParams;
import no.sharebox.api.methods.requests.LoginParams;
import no.sharebox.api.methods.requests.OpenLockerParams;
import no.sharebox.api.methods.requests.RegisterParams;
import no.sharebox.api.methods.requests.ReserveLockerParams;
import no.sharebox.api.methods.requests.StopSubscriptionParams;
import no.sharebox.api.methods.requests.VerifyPhoneParams;
import no.sharebox.api.methods.requests.VerifySmsCodeParams;
import no.sharebox.api.methods.responses.GetReservationData;
import no.sharebox.api.methods.responses.LoginData;
import no.sharebox.api.methods.responses.RegisterData;
import no.sharebox.api.methods.responses.ReserveLockerData;
import no.sharebox.api.methods.responses.VerifySmsCodeData;
import no.sharebox.sdk.tasks.BackgroundExecutor;
import no.sharebox.sdk.tasks.BackgroundTask;
import no.sharebox.sdk.tasks.CombinedTask;
import no.sharebox.sdk.tasks.CombinedTask.Combinator;
import no.sharebox.sdk.tasks.MainThreadHandler;
import no.sharebox.sdk.tasks.OnResultListener;
import no.sharebox.sdk.tasks.Subscription;
import no.sharebox.sdk.tasks.SubscriptionTask;
import no.sharebox.sdk.tasks.Task;
import no.sharebox.sdk.tasks.TaskRequest;

@AnyThread
public class ShareboxSdk {

    /**
     * Number of free hours for each reservation. If a reservation lasted longer it will be
     * automatically converted to paid one and user will have to install main Sharebox app
     * and provide credit card details before they can use Sharebox service again.
     */
    @SuppressWarnings("unused") // Public API
    public static final int FREE_HOURS = 12;

    /**
     * Link to terms and conditions page.
     */
    @SuppressWarnings("unused") // Public API
    public static final String TERMS_URL = "https://sharebox.no/terms/";

    /**
     * Support phone number for Norway.
     */
    @SuppressWarnings("unused") // Public API
    public static final String SUPPORT_PHONE_NO = "+4740434444";

    /**
     * Support phone number for Denmark.
     */
    @SuppressWarnings("unused") // Public API
    public static final String SUPPORT_PHONE_DK = "+4577340722";


    private static ShareboxSdk instance;

    private final MainThreadHandler mainThread = new MainThreadHandler();
    private final BackgroundExecutor executor = new BackgroundExecutor(mainThread);
    private final Preferences prefs;
    private final ShareboxApi api;

    private Subscription<UserProfile> profileSubscription;


    private ShareboxSdk(Context context, ShareboxApi.Options options) {
        prefs = new Preferences(context);
        api = ShareboxApi.init(options);
    }

    @SuppressWarnings("UnusedReturnValue") // Public API
    public static ShareboxSdk init(Context context, ShareboxApi.Options options) {
        return instance = new ShareboxSdk(context.getApplicationContext(), options);
    }

    public static ShareboxSdk get() {
        if (instance == null) {
            throw new NullPointerException(
                    "You need to initialize ShareboxSdk with \"init(Context, Options)\" method");
        }
        return instance;
    }


    /* ---------------------- */
    /* Local data methods     */
    /* ---------------------- */

    /**
     * Returns true if user is currently logged in.
     */
    public boolean isLoggedIn() {
        return prefs.getSessionId() != null;
    }

    /**
     * Returns true if phone number verification is in progress (sms code was requested).
     */
    public boolean isVerificationRequested() {
        return prefs.getPhoneToVerify() != null && prefs.getPhoneToken() == null;
    }

    /**
     * Returns phone number which is currently in verification phase (sms code was requested)
     * or if registration is in progress.
     */
    @Nullable
    public String getPhoneToVerify() {
        return prefs.getPhoneToVerify();
    }

    /**
     * Returns true if user successfully verified their phone number but didn't register yet.
     */
    public boolean isRegistrationNeeded() {
        return prefs.getPhoneToken() != null;
    }

    /**
     * Clears all user data.
     */
    public void logout() {
        prefs.saveSessionId(null);
        prefs.savePhoneToVerify(null);
        prefs.savePhoneToken(null);
        saveProfile(null);
    }

    private String getSessionOrThrow() {
        String sessionId = prefs.getSessionId();
        if (sessionId == null) {
            throw new ShareboxException("User is not logged in");
        }
        return sessionId;
    }


    /* ---------------------- */
    /* Subscriptions methods  */
    /* ---------------------- */

    /**
     * Returns user profile and subscribes to all consequent profile updates, including nulls
     * if user is not logged in.
     */
    public TaskRequest getProfileUpdates(OnResultListener<UserProfile> listener) {
        return new SubscriptionTask<>(getProfileSubscription()).onResult(listener).subscribe();
    }

    private void saveProfile(@Nullable UserProfile profile) {
        prefs.saveProfile(profile);
        getProfileSubscription().onResult(profile);
    }

    private Subscription<UserProfile> getProfileSubscription() {
        if (profileSubscription == null) {
            profileSubscription = new Subscription<>(mainThread); // Initializing subscription
            profileSubscription.onResult(prefs.getProfile());
        }
        return profileSubscription;
    }


    /* ---------------------- */
    /* API methods            */
    /* ---------------------- */

    /**
     * Requests latest consent info.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<Consent> getConsent() {
        return new BackgroundTask<>(executor, new Callable<Consent>() {
            @Override
            public Consent call() throws Exception {
                return api.getConsent();
            }
        });
    }

    /**
     * Requests phone verification code to be sent by SMS.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     *
     * @param phone Phone number to be verified.
     * @param consentVersion Granted consent version for current user.
     * @see #getConsent()
     */
    public Task<Void> verifyPhone(@NonNull final String phone, final int consentVersion) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (!ShareboxUtils.isValidPhoneFormat(phone)) {
                    throw new ShareboxException("Invalid phone format, use " +
                            "ShareboxUtils.isValidPhoneFormat() method to validate phone number.");
                }

                api.verifyPhone(new VerifyPhoneParams.Builder()
                        .phone(phone)
                        .deviceId(prefs.getDeviceId())
                        .consentVersion(consentVersion)
                        .build());

                prefs.savePhoneToVerify(phone);

                return null;
            }
        });
    }

    /**
     * Verifies user phone number (as was requested by {@link #verifyPhone(String, int)} method)
     * and tries to log in with this number.
     * <p>
     * Throws {@link ShareboxApiException} with error code {@link ShareboxApi#ERROR_NOT_REGISTERED}
     * if user should be asked to register first.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<Void> login(@NonNull final String smsCode) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                String phone = prefs.getPhoneToVerify();
                if (phone == null) {
                    throw new ShareboxException("You should request phone verification with " +
                            "ShareboxSdk.verifyPhone() before logging in with SMS code");
                }

                VerifySmsCodeData smsCodeData = api.verifySmsCode(new VerifySmsCodeParams.Builder()
                        .phone(phone)
                        .deviceId(prefs.getDeviceId())
                        .code(smsCode)
                        .build());

                prefs.savePhoneToken(smsCodeData.getPhoneToken());

                LoginData loginData = api.login(new LoginParams.Builder()
                        .phoneToken(prefs.getPhoneToken())
                        .build());

                // If user is not logged in login API will throw ShareboxApiException at this point

                saveProfile(loginData.getProfile());
                prefs.saveSessionId(loginData.getSessionId());
                prefs.savePhoneToken(null);
                prefs.savePhoneToVerify(null);

                return null;
            }
        });
    }

    /**
     * Registers the user and logs them in.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<Void> register(@NonNull final String firstName, @NonNull final String lastName,
            @NonNull final String email) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                String phoneToken = prefs.getPhoneToken();

                if (phoneToken == null) {
                    throw new ShareboxException("You should validate phone number and try to " +
                            "login first with ShareboxSdk.login() before you can register");
                }

                RegisterData data = api.register(new RegisterParams.Builder()
                        .phoneToken(phoneToken)
                        .firstName(firstName)
                        .lastName(lastName)
                        .email(email)
                        .build());

                String sessionId = data.getSessionId();
                UserProfile profile = api.getProfile(sessionId);

                saveProfile(profile);
                prefs.saveSessionId(sessionId);
                prefs.savePhoneToken(null);
                prefs.savePhoneToken(null);

                return null;
            }
        });
    }

    /**
     * Updates user profile details from server.
     * <p>
     * If returned profile has {@link UserProfile#isConsentGranted()} equals to false then we
     * should force user to grant their consent for the latest version as returned from
     * {@link #getConsent()} method.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<UserProfile> updateProfile() {
        return new BackgroundTask<>(executor, new Callable<UserProfile>() {
            @Override
            public UserProfile call() throws Exception {
                UserProfile profile = api.getProfile(getSessionOrThrow());
                saveProfile(profile);
                return profile;
            }
        });
    }

    /**
     * Saves version of latest granted consent into user profile.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     *
     * @param versionCode Granted consent version.
     * @see #getConsent()
     * @see UserProfile#isConsentGranted()
     */
    public Task<Void> grantConsent(final int versionCode) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                GrantConsentParams params = new GrantConsentParams.Builder()
                        .versionCode(versionCode)
                        .build();

                api.grantConsent(getSessionOrThrow(), params);

                // Updating user profile to ensure we have "consent granted" flag updated
                UserProfile profile = api.getProfile(getSessionOrThrow());
                saveProfile(profile);

                return null;
            }
        });
    }


    /**
     * Returns locations list (sorted by name).
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<List<StoreLocation>> getLocations() {
        return new BackgroundTask<>(executor, new Callable<List<StoreLocation>>() {
            @Override
            public List<StoreLocation> call() throws Exception {
                List<StoreLocation> list = api.getLocations(getSessionOrThrow());
                return ShareboxUtils.sortByName(list);
            }
        });
    }

    /**
     * Returns store location details, including cabinets and their availability.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<StoreLocation> getLocation(final long locationId) {
        return new BackgroundTask<>(executor, new Callable<StoreLocation>() {
            @Override
            public StoreLocation call() throws Exception {
                GetLocationParams params = new GetLocationParams.Builder()
                        .locationId(locationId)
                        .build();

                return api.getLocation(getSessionOrThrow(), params);
            }
        });
    }

    /**
     * Creates a new locker reservation.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<ReserveLockerData> reserveLocker(final long cabinetId,
            final String reservationName, final String phone) {

        return new BackgroundTask<>(executor, new Callable<ReserveLockerData>() {
            @Override
            public ReserveLockerData call() throws Exception {
                if (!ShareboxUtils.isValidPhoneFormat(phone)) {
                    throw new ShareboxException("Invalid phone format, use " +
                            "ShareboxUtils.isValidPhoneFormat() method to validate phone number.");
                }

                ReserveLockerParams params = new ReserveLockerParams.Builder()
                        .cabinetId(cabinetId)
                        .reservationName(reservationName)
                        .receiverPhone(phone)
                        .build();

                return api.reserveLocker(getSessionOrThrow(), params);
            }
        });
    }

    /**
     * Returns list of user's active reservations.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public Task<List<Reservation>> getReservationsActive() {
        return new BackgroundTask<>(executor, new Callable<List<Reservation>>() {
            @Override
            public List<Reservation> call() throws Exception {
                return api.getReservations(getSessionOrThrow());
            }
        });
    }

    /**
     * Returns list of recently finished user's reservations.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    @SuppressWarnings("WeakerAccess") // Public API
    public Task<List<Reservation>> getReservationsCompleted() {
        return new BackgroundTask<>(executor, new Callable<List<Reservation>>() {
            @Override
            public List<Reservation> call() throws Exception {
                return api.getReservationsHistory(getSessionOrThrow());
            }
        });
    }

    /**
     * Returns combined list of active and recently finished reservations.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     *
     * @see #getReservationsActive()
     * @see #getReservationsCompleted()
     */
    public Task<List<Reservation>> getReservationsAll() {
        return new CombinedTask<>(getReservationsActive(), getReservationsCompleted(),
                new Combinator<List<Reservation>, List<Reservation>, List<Reservation>>() {
                    @Override
                    public List<Reservation> combine(List<Reservation> r1, List<Reservation> r2) {
                        List<Reservation> all = new ArrayList<>();
                        if (r1 != null) {
                            all.addAll(r1);
                        }
                        if (r2 != null) {
                            all.addAll(r2);
                        }
                        return all;
                    }
                });
    }

    /**
     * Returns reservation's details.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<GetReservationData> getReservation(final long reservationId) {
        return new BackgroundTask<>(executor, new Callable<GetReservationData>() {
            @Override
            public GetReservationData call() throws Exception {
                GetReservationParams params = new GetReservationParams.Builder()
                        .reservationId(reservationId)
                        .build();

                return api.getReservation(getSessionOrThrow(), params);
            }
        });
    }

    /**
     * Stops a subscription.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<Void> stopSubscription(final long subscriptionId) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                StopSubscriptionParams params = new StopSubscriptionParams.Builder()
                        .subscriptionId(subscriptionId)
                        .build();

                api.stopSubscription(getSessionOrThrow(), params);

                return null;
            }
        });
    }

    /**
     * Opens a locker. Can optionally finish corresponding reservation.
     * <p>
     * <i>Note: this method will be executed asynchronously.</i>
     */
    public Task<Void> openLocker(final OpenLockerParams params) {
        return new BackgroundTask<>(executor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                api.openLocker(getSessionOrThrow(), params);
                return null;
            }
        });
    }

}
