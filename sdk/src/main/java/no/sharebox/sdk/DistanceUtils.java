package no.sharebox.sdk;

/**
 * All math is obtained from SphericalUtil class from android-maps-utils:
 * https://github.com/googlemaps/android-maps-utils/
 * blob/master/library/src/com/google/maps/android/SphericalUtil.java
 */
class DistanceUtils {

    private static final double EARTH_RADIUS = 6371009.0D;

    private DistanceUtils() {}

    /**
     * Returns the distance between two points, in meters.
     */
    static double computeDistanceBetween(double lat1, double lng1, double lat2, double lng2) {
        return computeAngleBetween(lat1, lng1, lat2, lng2) * EARTH_RADIUS;
    }

    /**
     * Returns the angle between two points, in radians. This is the same as the distance
     * on the unit sphere.
     */
    private static double computeAngleBetween(double lat1, double lng1, double lat2, double lng2) {
        return distanceRadians(Math.toRadians(lat1), Math.toRadians(lng1),
                Math.toRadians(lat2), Math.toRadians(lng2));
    }

    /**
     * Returns distance on the unit sphere; the arguments are in radians.
     */
    private static double distanceRadians(double lat1, double lng1, double lat2, double lng2) {
        return arcHav(havDistance(lat1, lat2, lng1 - lng2));
    }

    /**
     * Returns hav() of distance from (lat1, lng1) to (lat2, lng2) on the unit sphere.
     */
    private static double havDistance(double lat1, double lat2, double dLng) {
        return hav(lat1 - lat2) + hav(dLng) * Math.cos(lat1) * Math.cos(lat2);
    }

    /**
     * Returns haversine(angle-in-radians).
     * hav(x) == (1 - cos(x)) / 2 == sin(x / 2)^2.
     */
    private static double hav(double x) {
        double sinHalf = Math.sin(x * 0.5);
        return sinHalf * sinHalf;
    }

    /**
     * Computes inverse haversine. Has good numerical stability around 0.
     * arcHav(x) == acos(1 - 2 * x) == 2 * asin(sqrt(x)).
     * The argument must be in [0, 1], and the result is positive.
     */
    private static double arcHav(double x) {
        return 2 * Math.asin(Math.sqrt(x));
    }

}
