package no.sharebox.sdk.location;

import android.location.Location;
import android.support.annotation.NonNull;

public interface OnLocationChangedListener {

    void onLocationChanged(@NonNull Location location);

}
