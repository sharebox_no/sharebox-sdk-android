package no.sharebox.sdk;

import android.location.Location;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.sharebox.api.domain.StoreLocation;
import no.sharebox.sdk.location.LocationHelper;

public class ShareboxUtils {

    private ShareboxUtils() {}

    /**
     * Checks that phone number is in international format (starts with + sign).
     */
    public static boolean isValidPhoneFormat(String phone) {
        return phone != null && phone.startsWith("+");
    }

    /**
     * Computes distances from given location to each store.
     * <p>
     * See also {@link LocationHelper} - helper class to request location permissions and subscribe
     * to user location changes.
     */
    public static Map<StoreLocation, Integer> computeDistances(
            @NonNull List<StoreLocation> list, @NonNull Location location) {

        Map<StoreLocation, Integer> distances = new HashMap<>();

        for (StoreLocation store : list) {
            double distance = DistanceUtils.computeDistanceBetween(
                    store.getLatitude(), store.getLongitude(),
                    location.getLatitude(), location.getLongitude());
            distances.put(store, (int) Math.round(distance));
        }

        return distances;
    }

    /**
     * Sorts stores list by distance.
     * <p>
     * See {@link #computeDistances(List, Location)} method.
     */
    public static List<StoreLocation> sortByDistance(
            @NonNull final Map<StoreLocation, Integer> distances) {
        final List<StoreLocation> list = new ArrayList<>(distances.keySet());

        Collections.sort(list, new Comparator<StoreLocation>() {
            @Override
            public int compare(StoreLocation o1, StoreLocation o2) {
                return distances.get(o1).compareTo(distances.get(o2));
            }
        });

        return list;
    }

    static List<StoreLocation> sortByName(List<StoreLocation> list) {
        if (list == null) {
            return null;
        }

        final List<StoreLocation> sortedList = new ArrayList<>(list);

        Collections.sort(sortedList, new Comparator<StoreLocation>() {
            @Override
            public int compare(StoreLocation o1, StoreLocation o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return sortedList;
    }

}
