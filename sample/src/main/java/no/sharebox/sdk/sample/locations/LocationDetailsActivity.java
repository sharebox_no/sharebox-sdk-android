package no.sharebox.sdk.sample.locations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;

import no.sharebox.api.domain.Cabinet;
import no.sharebox.api.domain.StoreLocation;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.ShareboxUtils;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.reservations.ReservationsListActivity;

public class LocationDetailsActivity extends BaseActivity {

    private static final String EXTRA_LOCATION = "location";

    private ViewHolder views;
    private StoreLocation location;

    private boolean isLoadinLocation;
    private boolean isReserving;

    public static Intent createIntent(Context context, StoreLocation location) {
        Intent intent = new Intent(context, LocationDetailsActivity.class);
        intent.putExtra(EXTRA_LOCATION, new Gson().toJson(location));
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String jsonLocation = getIntent().getStringExtra(EXTRA_LOCATION);
        location = new Gson().fromJson(jsonLocation, StoreLocation.class);

        setContentView(R.layout.location_details_activity);
        getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);

        views = new ViewHolder(this);
        views.reserve.setOnClickListener(view -> reserveLocker());

        loadLocationDetails();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isReserving || isLoadinLocation) {
            addProgress(menu);
        }
        return true;
    }

    private void showLocationInfo() {
        views.locationInfo.showLocationInfo(location);

        boolean hasLockers = location.findAvailableCabinet() != null;
        views.reservationNotPossible.setVisibility(
                hasLockers || isLoadinLocation ? View.GONE : View.VISIBLE);
        views.locationLoading.setVisibility(isLoadinLocation ? View.VISIBLE : View.GONE);
    }


    private void loadLocationDetails() {
        isLoadinLocation = true;
        invalidateOptionsMenu();
        showLocationInfo();

        ShareboxSdk.get().getLocation(location.getId())
                .onResult(this::onLocationLoaded)
                .onError(this::onLocationError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onLocationLoaded(StoreLocation location) {
        isLoadinLocation = false;
        invalidateOptionsMenu();

        this.location = location;
        showLocationInfo();
    }

    private void onLocationError(Exception ex) {
        isLoadinLocation = false;
        invalidateOptionsMenu();
        showLocationInfo();

        ErrorsHandler.handle(this, ex);
    }


    private void reserveLocker() {
        String reservationName = views.reservationName.getText().toString();
        String phone = views.recipientPhone.getText().toString();
        Cabinet cabinet = location.findAvailableCabinet();

        if (TextUtils.isEmpty(reservationName)) {
            views.reservationName.setError(getString(R.string.error_required_field));
            return;
        } else if (TextUtils.isEmpty(phone)) {
            views.recipientPhone.setError(getString(R.string.error_required_field));
            return;
        } else if (!ShareboxUtils.isValidPhoneFormat(phone)) {
            views.recipientPhone.setError(getString(R.string.error_invalid_phone));
            return;
        } else if (cabinet == null) {
            return; // Should not be the case
        }

        isReserving = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().reserveLocker(cabinet.getId(), reservationName, phone)
                .onResult(result -> onReservationSuccess())
                .onError(this::onReservationError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onReservationSuccess() {
        isReserving = false;
        invalidateOptionsMenu();

        startActivity(new Intent(this, ReservationsListActivity.class));
        finish();
    }

    private void onReservationError(Exception ex) {
        isReserving = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
    }


    private static class ViewHolder {
        final LocationInfoView locationInfo;
        final EditText reservationName;
        final EditText recipientPhone;
        final View reserve;
        final View reservationNotPossible;
        final View locationLoading;

        ViewHolder(Activity activity) {
            this.locationInfo = activity.findViewById(R.id.location_info);
            this.reservationName = activity.findViewById(R.id.reservation_name);
            this.recipientPhone = activity.findViewById(R.id.recipient_phone);
            this.reserve = activity.findViewById(R.id.reserve);
            this.reservationNotPossible = activity.findViewById(R.id.reservation_not_possible);
            this.locationLoading = activity.findViewById(R.id.location_loading);
        }
    }

}
