package no.sharebox.sdk.sample.registration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.menu.MenuActivity;

public class RegistrationActivity extends BaseActivity {

    private ViewHolder views;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registration_activity);
        views = new ViewHolder(this);

        views.register.setOnClickListener(view -> register());
    }

    private void register() {
        final String firstName = views.firstName.getText().toString();
        final String lastName = views.lastName.getText().toString();
        final String email = views.email.getText().toString();

        if (TextUtils.isEmpty(firstName)) {
            views.firstName.setError(getString(R.string.error_required_field));
            return;
        } else if (TextUtils.isEmpty(lastName)) {
            views.lastName.setError(getString(R.string.error_required_field));
            return;
        } else if (TextUtils.isEmpty(email)) {
            views.email.setError(getString(R.string.error_required_field));
            return;
        }

        ShareboxSdk.get().register(firstName, lastName, email)
                .onResult(result -> onRegistered())
                .onError(ex -> ErrorsHandler.handle(this, ex))
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onRegistered() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }

    private static class ViewHolder {
        final EditText firstName;
        final EditText lastName;
        final EditText email;
        final View register;

        ViewHolder(Activity activity) {
            this.firstName = activity.findViewById(R.id.first_name);
            this.lastName = activity.findViewById(R.id.last_name);
            this.email = activity.findViewById(R.id.email);
            this.register = activity.findViewById(R.id.register);
        }
    }

}
