package no.sharebox.sdk.sample.locations;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import no.sharebox.api.domain.StoreLocation;
import no.sharebox.sdk.ShareboxUtils;
import no.sharebox.sdk.sample.R;

class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.ViewHolder> {

    private List<StoreLocation> origLocations;
    private List<StoreLocation> locations;
    private Location userLocation;
    private Map<StoreLocation, Integer> distances;

    void setLocations(List<StoreLocation> locations) {
        origLocations = locations;
        updateLocations();
    }

    void setUserLocation(Location location) {
        userLocation = location;
        updateLocations();
    }

    private void updateLocations() {
        if (origLocations != null) {
            if (userLocation != null) {
                // Sorting stores by distance
                distances = ShareboxUtils.computeDistances(origLocations, userLocation);
                locations = ShareboxUtils.sortByDistance(distances);
            } else {
                locations = origLocations;
            }
        } else {
            locations = null;
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_item, parent, false);
        itemView.setOnClickListener(this::onItemClick);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StoreLocation location = locations.get(position);
        final Context context = holder.itemView.getContext();

        holder.itemView.setTag(location);
        holder.name.setText(location.getName());
        holder.address.setText(location.getFullAddress());

        if (distances == null) {
            holder.distance.setVisibility(View.GONE);
        } else {
            holder.distance.setVisibility(View.VISIBLE);
            holder.distance.setText(formatDistance(context, distances.get(location)));
        }
    }

    @Override
    public int getItemCount() {
        return locations == null ? 0 : locations.size();
    }

    private void onItemClick(View view) {
        StoreLocation location = (StoreLocation) view.getTag();
        Context context = view.getContext();
        context.startActivity(LocationDetailsActivity.createIntent(context, location));
    }


    private String formatDistance(Context context, int distance) {
        if (distance < 1000) {
            return distance + " " + context.getString(R.string.unit_meters);
        } else if (distance < 10000) {
            return String.format(Locale.getDefault(), "%.1f", distance / 1000f)
                    + " " + context.getString(R.string.unit_kilometers);
        } else {
            return Math.round(distance / 1000f) + " " + context.getString(R.string.unit_kilometers);
        }
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final TextView address;
        final TextView distance;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.store_name);
            address = itemView.findViewById(R.id.store_address);
            distance = itemView.findViewById(R.id.store_distance);
        }
    }

}
