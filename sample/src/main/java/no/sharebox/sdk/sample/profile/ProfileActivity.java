package no.sharebox.sdk.sample.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import no.sharebox.api.domain.UserProfile;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;

public class ProfileActivity extends BaseActivity {

    private ViewHolder views;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.profile_activity);
        getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);
        views = new ViewHolder(this);

        // Getting user profile and subscribing to all consequent profile updates
        ShareboxSdk.get().getProfileUpdates(this::showProfile).cancelOnDestroy(this);

        // Requesting profile update from server
        ShareboxSdk.get().updateProfile().subscribe();
    }

    private void showProfile(UserProfile profile) {
        if (profile != null) {
            views.firstName.setText(profile.getFirstName());
            views.lastName.setText(profile.getLastName());
            views.phone.setText(profile.getPhone());
            views.email.setText(profile.getEmail());
        }
    }


    private static class ViewHolder {
        final TextView firstName;
        final TextView lastName;
        final TextView phone;
        final TextView email;

        ViewHolder(Activity activity) {
            this.firstName = activity.findViewById(R.id.profile_fist_name);
            this.lastName = activity.findViewById(R.id.profile_last_name);
            this.phone = activity.findViewById(R.id.profile_phone);
            this.email = activity.findViewById(R.id.profile_email);
        }
    }

}
