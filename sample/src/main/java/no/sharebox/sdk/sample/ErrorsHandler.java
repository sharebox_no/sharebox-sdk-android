package no.sharebox.sdk.sample;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

import no.sharebox.api.exceptions.ShareboxApiException;

public class ErrorsHandler {

    private ErrorsHandler() {}

    public static void handle(Context context, Exception ex) {
        if (isConnectionError(ex)) {
            Toast.makeText(context, R.string.error_connection, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, R.string.error_general, Toast.LENGTH_SHORT).show();
        }

        Log.e("Sharebox", "Error occurred", ex);
    }

    public static boolean isApiError(Exception ex, int code) {
        return ex instanceof ShareboxApiException && ((ShareboxApiException) ex).getCode() == code;
    }

    private static boolean isConnectionError(Throwable throwable) {
        return throwable instanceof SocketException
                || throwable instanceof SSLException
                || throwable instanceof UnknownHostException
                || throwable instanceof InterruptedIOException;
    }

}
