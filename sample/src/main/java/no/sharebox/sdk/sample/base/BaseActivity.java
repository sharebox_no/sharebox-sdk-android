package no.sharebox.sdk.sample.base;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import no.sharebox.sdk.sample.R;

public abstract class BaseActivity extends AppCompatActivity {

    @NonNull
    protected ActionBar getSupportActionBarNotNull() {
        // Making getSupportActionBar() method to be @NonNull
        ActionBar actionBar = super.getSupportActionBar();
        if (actionBar == null) {
            throw new NullPointerException("Action bar was not initialized");
        }
        return actionBar;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default: // No-op
        }
        return true;
    }

    protected void addProgress(Menu menu) {
        MenuItem progress = menu.add(Menu.NONE, Menu.NONE, Menu.NONE, null);
        progress.setActionView(R.layout.toolbar_progress);
        progress.setEnabled(false);
        progress.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

}
