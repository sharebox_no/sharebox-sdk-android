package no.sharebox.sdk.sample.reservations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import java.util.List;

import no.sharebox.api.domain.Reservation;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;

public class ReservationsListActivity extends BaseActivity {

    private ReservationsAdapter adapter;
    private boolean isLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reservations_activity);
        getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);

        RecyclerView list = findViewById(R.id.reservations);
        list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ReservationsAdapter();
        list.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadReservations();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isLoading) {
            addProgress(menu);
        }
        return true;
    }

    private void loadReservations() {
        isLoading = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().getReservationsAll()
                .onResult(this::onReservationsLoaded)
                .onError(this::onReservationsError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onReservationsLoaded(List<Reservation> reservations) {
        isLoading = false;
        invalidateOptionsMenu();

        adapter.setReservations(reservations);
    }

    private void onReservationsError(Exception ex) {
        isLoading = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
    }

}
