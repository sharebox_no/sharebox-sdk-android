package no.sharebox.sdk.sample.start;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.login.LoginActivity;
import no.sharebox.sdk.sample.menu.MenuActivity;
import no.sharebox.sdk.sample.registration.RegistrationActivity;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ShareboxSdk.get().isLoggedIn()) {
            // Opening main menu screen
            startActivity(new Intent(this, MenuActivity.class));
        } else if (ShareboxSdk.get().isRegistrationNeeded()) {
            // Opening registration screen
            startActivity(new Intent(this, RegistrationActivity.class));
        } else {
            // Opening login screen
            startActivity(new Intent(this, LoginActivity.class));
        }

        finish();
    }

}
