package no.sharebox.sdk.sample;

import android.app.Application;

import no.sharebox.api.ShareboxApi;
import no.sharebox.sdk.ShareboxSdk;

public class SampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ShareboxSdk.init(this, new ShareboxApi.Options()
                .baseUrl(BuildConfig.API_URL)
                .credentials(BuildConfig.API_LOGIN, BuildConfig.API_PASSWORD)
                .debugLogs(BuildConfig.DEBUG));
    }

}
