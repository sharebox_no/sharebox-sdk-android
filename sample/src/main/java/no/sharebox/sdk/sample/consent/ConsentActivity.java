package no.sharebox.sdk.sample.consent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import no.sharebox.api.domain.Consent;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;

public class ConsentActivity extends BaseActivity {

    private static final String EXTRA_VERSION = "version";

    private ViewHolder views;
    private boolean isLoading;
    private Consent consent;

    public static int getVersion(Intent data) {
        return data.getIntExtra(EXTRA_VERSION, 0);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.consent_activity);

        views = new ViewHolder(this);
        views.cancel.setOnClickListener(view -> onCancel());
        views.accept.setOnClickListener(view -> onAccept());

        if (ShareboxSdk.get().isLoggedIn()) {
            views.cancel.setText(R.string.button_logout);
        } else {
            getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);
            setResult(RESULT_CANCELED);
        }

        loadConsent();
    }

    @Override
    public void onBackPressed() {
        // Should not be possible to dismiss activity if user must grant new consent
        if (!ShareboxSdk.get().isLoggedIn()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isLoading) {
            addProgress(menu);
        }
        return true;
    }

    private void onCancel() {
        if (isLoading) {
            return;
        }

        if (ShareboxSdk.get().isLoggedIn()) {
            // Logging out
            ShareboxSdk.get().logout();
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private void onAccept() {
        if (isLoading) {
            return;
        }

        if (ShareboxSdk.get().isLoggedIn()) {
            grantConsent();
        } else {
            Intent data = new Intent();
            data.putExtra(EXTRA_VERSION, consent.getVersionCode());
            setResult(RESULT_OK, data);
            finish();
        }
    }


    private void loadConsent() {
        isLoading = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().getConsent()
                .onResult(this::onConsentLoaded)
                .onError(this::onError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onConsentLoaded(Consent consent) {
        isLoading = false;
        invalidateOptionsMenu();

        this.consent = consent;

        views.text.setText(Html.fromHtml(consent.getContent()));
        views.layout.setVisibility(View.VISIBLE);
    }

    private void grantConsent() {
        isLoading = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().grantConsent(consent.getVersionCode())
                .onResult(result -> onConsentGranted())
                .onError(this::onError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onConsentGranted() {
        isLoading = false;
        invalidateOptionsMenu();

        finish();
    }

    private void onError(Exception ex) {
        isLoading = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
        finish();
    }


    private static class ViewHolder {
        final View layout;
        final TextView text;
        final Button cancel;
        final Button accept;

        ViewHolder(Activity activity) {
            this.layout = activity.findViewById(R.id.consent_layout);
            this.text = activity.findViewById(R.id.consent_text);
            this.cancel = activity.findViewById(R.id.consent_cancel);
            this.accept = activity.findViewById(R.id.consent_accept);
        }
    }
}
