package no.sharebox.sdk.sample.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import no.sharebox.api.domain.UserProfile;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.consent.ConsentActivity;
import no.sharebox.sdk.sample.locations.LocationsListActivity;
import no.sharebox.sdk.sample.profile.ProfileActivity;
import no.sharebox.sdk.sample.reservations.ReservationsListActivity;
import no.sharebox.sdk.sample.start.StartActivity;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu_activity);
        ViewHolder views = new ViewHolder(this);

        views.locations.setOnClickListener(view -> openLocations());
        views.reservations.setOnClickListener(view -> openReservations());
        views.profile.setOnClickListener(view -> openProfile());
        views.logout.setOnClickListener(view -> logout());

        // Silently updating user profile on start up
        ShareboxSdk.get().updateProfile()
                .onResult(this::onProfileUpdated)
                .subscribe()
                .cancelOnDestroy(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!ShareboxSdk.get().isLoggedIn()) {
            // Returning to login screen
            startActivity(new Intent(this, StartActivity.class));
            finish();
        }
    }

    private void openLocations() {
        startActivity(new Intent(this, LocationsListActivity.class));
    }

    private void openReservations() {
        startActivity(new Intent(this, ReservationsListActivity.class));
    }

    private void openProfile() {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    private void logout() {
        ShareboxSdk.get().logout();
        startActivity(new Intent(this, StartActivity.class));
        finish();
    }


    private void onProfileUpdated(UserProfile profile) {
        if (!profile.isConsentGranted()) {
            // User must grant new consent or log out
            startActivity(new Intent(this, ConsentActivity.class));
        }
    }


    private static class ViewHolder {
        final View locations;
        final View reservations;
        final View profile;
        final View logout;

        ViewHolder(Activity activity) {
            this.locations = activity.findViewById(R.id.menu_locations);
            this.reservations = activity.findViewById(R.id.menu_reservations);
            this.profile = activity.findViewById(R.id.menu_profile);
            this.logout = activity.findViewById(R.id.menu_logout);
        }
    }

}
