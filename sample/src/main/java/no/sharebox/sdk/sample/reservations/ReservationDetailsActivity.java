package no.sharebox.sdk.sample.reservations;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import no.sharebox.api.ShareboxApi;
import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.ReservationEvent;
import no.sharebox.api.domain.ReservationUser;
import no.sharebox.api.methods.requests.OpenLockerParams;
import no.sharebox.api.methods.responses.GetReservationData;
import no.sharebox.sdk.ShareboxSdk;
import no.sharebox.sdk.location.LocationHelper;
import no.sharebox.sdk.sample.ErrorsHandler;
import no.sharebox.sdk.sample.R;
import no.sharebox.sdk.sample.base.BaseActivity;
import no.sharebox.sdk.sample.locations.LocationInfoView;

public class ReservationDetailsActivity extends BaseActivity {

    private static final String EXTRA_RESERVATION = "reservation";

    private final SimpleDateFormat formatter =
            new SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale.getDefault());

    private ViewHolder views;
    private Reservation reservation;
    private List<ReservationEvent> events;
    private boolean isLoading;

    private LocationHelper locationHelper;
    private Location location;


    public static Intent createIntent(Context context, Reservation reservation) {
        Intent intent = new Intent(context, ReservationDetailsActivity.class);
        intent.putExtra(EXTRA_RESERVATION, new Gson().toJson(reservation));
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String jsonReservation = getIntent().getStringExtra(EXTRA_RESERVATION);
        reservation = new Gson().fromJson(jsonReservation, Reservation.class);

        setContentView(R.layout.reservation_details_activity);
        getSupportActionBarNotNull().setDisplayHomeAsUpEnabled(true);

        views = new ViewHolder(this);
        views.open.setOnClickListener(view -> openLocker(false, false));
        views.finish.setOnClickListener(view -> showFinishReservationOptions());

        showReservationDetails();
        loadDetails();

        locationHelper = new LocationHelper(this, this::onUserLocationChanged);
        locationHelper.autoConnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isLoading) {
            addProgress(menu);
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        boolean handled = locationHelper.handlePermissionsResult(requestCode, grantResults);

        if (!handled) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void onUserLocationChanged(@NonNull Location location) {
        this.location = location;
    }


    private void showReservationDetails() {
        views.locationInfo.showLocationInfo(reservation.getLocation());

        setTextWithFallback(views.reservationName, reservation.getName());

        // Target user (recipient or owner)
        ReservationUser targetUser;
        if (reservation.isFromYou()) {
            targetUser = reservation.getRecipients().isEmpty()
                    ? null : reservation.getRecipients().get(0);
        } else {
            targetUser = reservation.getOwner();
        }

        String targetName = targetUser == null ? null : targetUser.getName();
        String targetPhone = targetUser == null ? null : targetUser.getPhone();
        String target = targetName == null ? targetPhone : targetName + " (" + targetPhone + ")";
        setTextWithFallback(views.recipient, target);

        // Reservation actions
        views.finish.setVisibility(reservation.canFinish() ? View.VISIBLE : View.GONE);
        views.open.setEnabled(reservation.canOpen());
        views.finish.setEnabled(reservation.canOpen());

        // Reservation events
        views.events.removeAllViews();

        if (events != null) {
            for (ReservationEvent event : events) {
                addEventView(event, views.events);
            }
            views.eventsPlaceholder.setVisibility(events.isEmpty() ? View.VISIBLE : View.GONE);
        }
    }

    private void setTextWithFallback(TextView view, String text) {
        view.setText(TextUtils.isEmpty(text) ? "-" : text);
    }

    private void addEventView(ReservationEvent event, ViewGroup parent) {
        View item = LayoutInflater.from(this).inflate(R.layout.reservation_event, parent, false);

        TextView text = item.findViewById(R.id.event_text);
        TextView date = item.findViewById(R.id.event_date);

        text.setText(getEventDescription(event));
        date.setText(formatter.format(event.getDate()));

        parent.addView(item);
    }

    private String getEventDescription(ReservationEvent event) {
        String name = event.getUser().getName();
        String phone = event.getUser().getPhone();
        String userInfo = TextUtils.isEmpty(name) ? phone : name + " (" + phone + ")";

        int eventTextId;
        switch (event.getType()) {
            case CREATED:
                eventTextId = R.string.event_created;
                break;
            case OPENED:
                eventTextId = R.string.event_opened;
                break;
            default:
                throw new RuntimeException("Unknown event type: " + event.getType());
        }

        return getString(eventTextId, userInfo);
    }


    private void loadDetails() {
        isLoading = true;
        invalidateOptionsMenu();

        ShareboxSdk.get().getReservation(reservation.getId())
                .onResult(this::onDetailsLoaded)
                .onError(this::onDetailsError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onDetailsLoaded(GetReservationData data) {
        isLoading = false;
        invalidateOptionsMenu();

        reservation = data.getReservation();
        events = data.getEvents();
        showReservationDetails();
    }

    private void onDetailsError(Exception ex) {
        isLoading = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
    }


    private void showFinishReservationOptions() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.message_finish_reservation_prompt)
                .setPositiveButton(R.string.button_finish_reservation,
                        (dialogInterface, i) -> finishReservation())
                .setNegativeButton(R.string.button_open_locker_and_finish_reservation,
                        (dialogInterface, i) -> openLocker(false, true))
                .show();
    }

    private void finishReservation() {
        ShareboxSdk.get()
                .stopSubscription(reservation.getSubscriptionId())
                .onResult(result -> onFinishReservationSuccess())
                .onError(this::onFinishReservationError)
                .subscribe()
                .cancelOnDestroy(this);
    }

    private void onFinishReservationSuccess() {
        isLoading = false;
        invalidateOptionsMenu();

        Toast.makeText(this, R.string.message_reservation_finished, Toast.LENGTH_SHORT).show();

        loadDetails(); // Updating reservation status and events list
    }

    private void onFinishReservationError(Exception ex) {
        isLoading = false;
        invalidateOptionsMenu();

        ErrorsHandler.handle(this, ex);
    }

    private void openLocker(boolean force, boolean finish) {
        if (location == null && !force) {
            // Warning user that we can't access their location
            warnUserNoLocation(finish);
        } else {
            isLoading = true;
            invalidateOptionsMenu();

            double lat = location == null ? 0.0 : location.getLatitude();
            double lon = location == null ? 0.0 : location.getLongitude();

            OpenLockerParams params = new OpenLockerParams.Builder()
                    .lockerId(reservation.getLockerId())
                    .location(lat, lon)
                    .forceOpen(force)
                    // If user is the owner of the reservation then this method should open the
                    // locker but keep the reservation. If user is the recipient then the
                    // reservation will be finished regardless of this parameter.
                    .finishReservation(finish)
                    .build();

            ShareboxSdk.get()
                    .openLocker(params)
                    .onResult(result -> onOpenLockerSuccess(finish))
                    .onError(ex -> onOpenLockerError(ex, finish))
                    .subscribe()
                    .cancelOnDestroy(this);
        }
    }

    private void onOpenLockerSuccess(boolean finish) {
        isLoading = false;
        invalidateOptionsMenu();

        int msgId = finish ? R.string.message_reservation_finished : R.string.message_locker_open;
        Toast.makeText(this, msgId, Toast.LENGTH_SHORT).show();

        loadDetails(); // Updating reservation status and events list
    }

    private void onOpenLockerError(Exception ex, boolean finish) {
        isLoading = false;
        invalidateOptionsMenu();

        if (ErrorsHandler.isApiError(ex, ShareboxApi.ERROR_TOO_FAR_FROM_LOCKER)) {
            warnUserTooFar(finish);
        } else {
            ErrorsHandler.handle(this, ex);
        }
    }


    private void warnUserTooFar(boolean finish) {
        int msgId = finish ? R.string.message_force_finish : R.string.message_force_open;
        showLocationWarningDialog(msgId, finish);
    }

    private void warnUserNoLocation(boolean finish) {
        showLocationWarningDialog(R.string.message_force_open_no_location, finish);
    }

    private void showLocationWarningDialog(@StringRes int msgId, boolean finish) {
        new AlertDialog.Builder(this)
                .setMessage(msgId)
                .setPositiveButton(finish
                                ? R.string.button_open_locker_and_finish_reservation
                                : R.string.button_open_locker,
                        (dialogInterface, i) -> openLocker(true, finish))
                .setNegativeButton(R.string.button_cancel, null)
                .show();
    }


    private static class ViewHolder {
        final LocationInfoView locationInfo;
        final TextView reservationName;
        final TextView recipient;
        final View open;
        final View finish;
        final ViewGroup events;
        final View eventsPlaceholder;

        ViewHolder(Activity activity) {
            this.locationInfo = activity.findViewById(R.id.location_info);
            this.reservationName = activity.findViewById(R.id.reservation_name);
            this.recipient = activity.findViewById(R.id.recipient);
            this.open = activity.findViewById(R.id.open);
            this.finish = activity.findViewById(R.id.finish);
            this.events = activity.findViewById(R.id.events);
            this.eventsPlaceholder = activity.findViewById(R.id.events_placeholder);
        }
    }

}
