package no.sharebox.sdk.sample.reservations;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import no.sharebox.api.domain.Reservation;
import no.sharebox.api.domain.ReservationUser;
import no.sharebox.sdk.sample.R;

class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {

    private final SimpleDateFormat formatter =
            new SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale.getDefault());

    private List<Reservation> reservations;

    void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reservation_item, parent, false);
        itemView.setOnClickListener(this::onItemClick);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        Reservation reservation = reservations.get(position);

        holder.itemView.setTag(reservation);

        // Target user (recipient or owner)
        ReservationUser targetUser;
        int recipientTextId;

        if (reservation.isFromYou()) {
            targetUser = reservation.getRecipients().isEmpty()
                    ? null : reservation.getRecipients().get(0);
            recipientTextId = R.string.recipient_outgoing;
        } else {
            targetUser = reservation.getOwner();
            recipientTextId = R.string.recipient_incoming;
        }

        String name = targetUser == null ? null : targetUser.getName();
        if (TextUtils.isEmpty(name)) {
            name = targetUser == null ? null : targetUser.getPhone();
        }
        holder.recipient.setText(context.getString(recipientTextId, name));

        // Location and time
        holder.location.setText(reservation.getLocation().getName());
        holder.date.setText(formatter.format(reservation.getDate()));

        holder.status.setImageResource(reservation.isActive()
                ? R.drawable.ic_access_time_black_24dp : R.drawable.ic_done_black_24dp);
    }

    @Override
    public int getItemCount() {
        return reservations == null ? 0 : reservations.size();
    }

    private void onItemClick(View view) {
        Context context = view.getContext();
        Reservation reservation = (Reservation) view.getTag();
        context.startActivity(ReservationDetailsActivity.createIntent(context, reservation));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView recipient;
        final TextView location;
        final TextView date;
        final ImageView status;

        ViewHolder(View itemView) {
            super(itemView);

            recipient = itemView.findViewById(R.id.recipient);
            location = itemView.findViewById(R.id.location);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status_icon);
        }
    }

}
